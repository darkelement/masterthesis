\section{Genetic algorithms\label{sec:genetic_algorithms}}

Genetic algorithm is stochastic algorithm aimed for solving optimization and search problems inspired by biological processes. In genetic algorithms we use collection of possible solutions of given problem which are represented by data stored in computers memory. This data are subjected to operations like mutation, crossover and selection which produce new generation better suited for solving of given problem. This underlying concepts are relatively simple yet combined together constitute powerful tool whose effects we can observe in surrounding astonishing diversity of animate nature.

Run-flow of genetic algorithm looks as follows:
\begin{enumerate}
	\item generating random initial population;
	\item evaluating fitness of every member of population;
	\item if satisfying solution was found algorithm stops;
	\item choosing best individuals to create new generation from them;
	\item performing mutations on new generation;
	\item return to point 2.
\end{enumerate}

\paragraph{Encoding}
Before starting to code one has to decide how the \memph{genotype} of individuals will be represented, that is how to store in computers memory data characterising possible solutions of ones problem, a \memph{phenotype}, and eventual auxiliary information. Usually used data structure is array of numbers or bits called \memph{chromatin}\footnote{More popular in literature term is \memph{chromosome}, but chromatin is an actual biological analogy.}.

\paragraph{Selection}
To find possibly most adequate solution we choose from whole population best suited individuals and basing on their genotype construct new generation. E.g. if we search for minimum of a function we would prefer individuals who provide lower evaluation of this function; if we try to construct an electric circuit of given characteristic we would prefer individuals who provide this characteristic and use low number of elements.

There are many methods of selection. Let's describe two of them:
\begin{itemize}
	\item roulette method --- individuals are randomly chosen from population with probability depending on their fitness. In this method best individuals have biggest probability to reproduce, but with each step, when population becomes less diverse speed of evolution slows;
	\item ranking method --- in this method individuals are placed in queue sorted depending on their fitness. Part of queue is rejected and only best suited individuals are allowed to reproduce. Pace of slowing down of evolution is not so fast as in above method, but it may happen that also individuals whose fitness is low take part in reproduction.
\end{itemize}
Clearly both methods may be mixed if there is more than one criterion of fitness.

Since this is stochastic process there is no warranty we will find better solution in next generation, so best suited individual should be preserved to avoid regression.

\paragraph{Evolution operators} In order to generate next, possibly better suited, generation we need to somehow ,,transform'' existing population. We use two operations:
\begin{itemize}
	\item \emph{crossover} --- aim of this operation is to combine random fragments of genotype of two individuals to create new ones hoping that offspring will inherit best properties of its parents. The way combine genotype depends on the way it is decoded. If data are decoded in form of array, the parents arrays are cut randomly on two (or more) pieces and then joined like on below scheme:
		\begin{center}	
			\begin{tabular}{|c|c|c|c|c|c|}\hline
				\textbf{1}&\textbf{1}&\textbf{1}&0&0&1\\\hline
			\end{tabular}
			+ 
			\begin{tabular}{|c|c|c|c|c|c|}\hline
				0&1&1&\textbf{1}&\textbf{1}&\textbf{0}\\\hline
			\end{tabular}
			=
			\begin{tabular}{|c|c|c|c|c|c|}\hline
				1&1&1&1&1&0\\\hline
			\end{tabular}
		\end{center}
		Crossover only produces mixes of existing \memph{genes} and can not introduce new ones to the system. To avoid situation when genes coding necessary properties are not available we use mutation.
	\item \emph{mutation} --- aim of this operation is to introduce more diversity into populations genotype. In this process we introduce small random changes to individuals genotype. E.g. if data are stored in binary form we can perform negation on fragment of chromatin fiber; in this case too frequent or too aggressive mutation may destroy results of selection, so its probability should be rather small, around 1\%. If data are stored as real numbers some of them may be modified by small amount taken from some probability distribution; since this this does not influence individuals much, the probabily of mutation should be rather hight. There may be also introduced variation where parameters of mutation change in time or even are subject of evolution itself.
\end{itemize}

The way how data are represented and manipulated has a great meaning for algorithms speed and adequacy of solutions, since these have leading influence on how the space of possible solutions is traversed.

Genetic algorithms are excellent tool for solving problems, where only approximate solution is needed, and exact solution is hard or inefficient to find. Good example is traveling salesmen problem which belongs to a class of $NP$-hard problems. Time of finding exact solution grows exponentially with size of input data, so solving problem in practise becomes impossible, thought one can find approximate solution in acceptable time using genetic algorithms.

