\section{Entanglement measures of mixed states}

Unlike in case of pure state, there is no entanglement measure of mixed states that is generally agreed to be suitable. There are some propositions of measures, but each of them has some drawbacks.

First generalisation of entropy of entanglement to mixed states that may come to mind is \emph{entanglement of formation} $E_f$. It is defined as minimum of convex combination of entropies of entanglement taken over all possible decompositions of given state $\hat\rho$ to pure states:
\begin{equation}
	E_f(\hat\rho) = \min\limits_{\hat\rho = \sum_ip_i\ket{\psi_i}\bra{\psi_i}} \sum\limits_j p_j E_{ent}(\ket{\psi_j}\bra{\psi_j})
\end{equation}
In general this minimization is hard to perform, but fortunately for pair of qubits it can be expressed as:
\begin{equation}
	E_f(\hat\rho) = -x\lg x - (1-x)\lg (1-x),
\end{equation}
where
\begin{equation}
	x = \frac{1 + \sqrt{1-C^2}}{2}
\end{equation}
and
\begin{equation}
	C(\hat\rho) = \max\{0, \lambda_1 - \lambda_2 - \lambda_3 - \lambda_4\},
\end{equation}
where $\lambda$'s are square roots of eigenvalues of matrix $\tilde\rho = \hat\rho(\sigma_y\otimes\sigma_y)\hat\rho^*(\sigma_y\otimes\sigma_y)$ in descending order.

Another approach is to define entanglement measures using distance functions:
\begin{equation}
	\label{eq:measure_mixed}
	E(\hat\rho) = \min\limits_{\hat\sigma \in \Separable} D(\hat\rho\|\hat\sigma)
\end{equation}
where minimum is taken over all possible separable states. In order to let $E$ satisfy conditions (E1)--(E3) $D$ must satisfy \cite{vedral_plenio_1998}:
\begin{enumerate}[leftmargin=*,labelindent=1em,label=\textbf{(D\theenumi)}]
	\item $D(\hat\rho\|\hat\sigma) \geq 0$ with equality saturated if and only if $\hat\rho=\hat\sigma$;
	\item unitary operation leave $D(\hat\rho\|\hat\sigma)$ invariant, i.e. $D(\hat\rho\|\hat\sigma) = D(U\hat\rho U^\dagger \| U\hat\sigma U^\dagger)$;
	\item $D(\tr_A\hat\rho \| \tr_A\hat\sigma) \leq D(\hat\rho \| \hat\sigma)$;
	\item $\sum_i p_i D(\hat\rho_i/p_i \| \hat\sigma_i/q_i) \leq \sum_i D(\hat\rho_i \| \hat\sigma_i)$, where $p_i = \tr(\hat\rho_i)$, $q_i = \tr(\hat\sigma_i)$, $\hat\sigma_i = V_i\hat\sigma V_i^\dagger$ and $\hat\rho_i = V_i\hat\rho V_i^\dagger$;
	\item
		\begin{enumerate}
			\item $D(\sum_i \hat P_i\hat\rho\hat P_i \| \sum_i \hat P_i \hat\sigma P_i) = \sum_i D(\hat P_i\hat\rho\hat P_i \| \hat P_i\hat\sigma P_i)$, where $\hat P_i$ is any set of orthonormal projectors such that $\hat P_i \hat P_j = \delta_{ij} P_j$ or
			\item $D(\hat\rho \otimes \hat P_\alpha \| \hat\sigma \otimes \hat P_\alpha) = D(\hat\rho \| \hat\sigma)$, where $P_\alpha$ is any projector.
		\end{enumerate}
\end{enumerate}

Function $D(\hat\rho \| \hat\sigma)$ has intuitive geometrical interpretation as distance between state $\hat\rho$ and $\hat\sigma$, therefore entanglement defined as (\ref{eq:measure_mixed}) has interpretation of distance between $\hat\rho$ and set of separable quantum states $\Separable$ as showed on figure (\ref{fig:measures}). It has to be remembered that $D$ is not distance in mathematical sense since it does not posses properties of metric.

\begin{figure}[hbt]
	\centering{
		\begin{tikzpicture}
			\draw[line width=1pt] (0, 0) circle (1.5in);
			\node (S) at (290:0.7in) {$\Separable$};
			\draw[line width=1pt] (0, 0) circle (0.8in);
			\node (E) at (290:1.4in) {$\States$};
			\draw[line width=1pt,arrows=<->] (135:0.83in) -- (135:1.27in) node[pos=0.8,right] {$D(\hat\rho \| \hat\rho^*)$};
			\draw[line width=1pt,arrows=<->] (135:0.03in) -- (135:0.77in) node[pos=0.7,right] {$D(\hat\rho^* \| \hat\sigma)$};
			\fill[color=black] (0.0in, 0.0in) circle (2pt) node[below,color=black] {$\hat\sigma$};
			\fill[color=black] (135:0.8in) circle (2pt) node[left,color=black] {$\hat\rho^*\hspace{2pt}$};
			\fill[color=black] (135:1.3in) circle (2pt) node[above,color=black] {$\hat\rho$};
		\end{tikzpicture}
	}
	\caption{Schematic representation of all quantum states $\States$ and separable states $\Separable$. Point $\hat\rho^*$ represents the nearest separable state to state $\hat\rho$. Distance $D(\hat\rho \| \hat\rho^*)$ represents amount of quantum correlations in $\hat\rho$, $D(\hat\rho^* \|\hat\sigma)$ represents amount of classical correlations in this state (see Verdal and Plenio \cite{vedral_plenio_1998,plenio_vedral_1998}).\label{fig:measures}}
\end{figure}

Some functions satisfying conditions (D1)--(D5) will now be described in more details.

\subsection{Relative entropy}

Relative entropy is given by following equantion:
\begin{equation}
	D_{re}(\hat\rho \| \hat\sigma) \eqdef \tr\left(\hat\rho (\log\hat\rho - \log\hat\sigma)\right).
\end{equation}
It is a quantum generalisation of Kullback-Leibler divergence.

\subsection{Hilbert-Schmidt distance}

The Hilbert-Schmidt distance between any two density operators is defined as the Hilbert-Schmidt norm of their difference:
\begin{equation}
	D_{HS}(\hat\rho \| \hat\sigma) \eqdef \sqrt{\tr((\hat\rho - \hat\sigma)^2)}.
\end{equation}
It not only satisfies properties (D1)--(D3) but is also a metric which induces the flat, Euclidean geometry into the set of mixed states.

\subsection{Bures distance}

Another widely used distance measure is Bures distance, given by: 
\begin{equation}
	D_{B}(\hat\rho \| \hat\sigma)
	\eqdef \sqrt{2-2\sqrt{F(\hat\rho, \hat\sigma)}}
	= \sqrt{2-2\tr\sqrt{\sqrt{\hat\sigma}\hat\rho\sqrt{\hat\sigma}}}.
\end{equation}
While Hilbert-Schmidt metric induces flat geometry, Bures metric stretches Bloch ball to a hiper-hemisphere, i.e.
\begin{equation}
	D_{B}(\hat\rho(\vec r) \| \hat\sigma(\vec p)) =
	\left\| \left(r_1,r_2,r_3,\sqrt{1-|\vec r|^2}\right) - \left(p_1,p_2,p_3,\sqrt{1-|\vec p|^2}\right) \right\|.
\end{equation}

Using superfidelity one can also introduce modified Bures distance:
\begin{equation}
	\begin{split}
		D_{G}(\hat\rho \| \hat\sigma)
		\eqdef &\sqrt{2-2\sqrt{G(\hat\rho, \hat\sigma)}} =\\
		= &\sqrt{2-2\sqrt{\tr(\hat\rho\hat\sigma) + \sqrt{1-\tr\hat\sigma^2} \sqrt{1-\tr\hat\rho^2}}}.
	\end{split}
\end{equation}

