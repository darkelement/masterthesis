\section{Quantification of entanglement}

In practical applications it is important to know exactly how many weakly entangled particles one needs to produce maximally entangled pairs to reduce noise in communication channel and provide seamless quantum communication. Before we proceed to methods of quantifying entanglement let's precisely define circumstances of our actions. They consist of:
\begin{enumerate}
	\item \textbf{Local general measurements} (LGM) --- operations are local, i.e. operations in one system do not influence other systems.
	\item \textbf{Classical communication} (CC) --- actions performed on systems $A$ and $B$ may be correlated, but only classically (e.g. telephone connection between experimenters).
	\item \textbf{Postselection} (PS) --- performed after all other operations, mathematically it corresponds to not complete general measurement (e.g. in distillation procedure described in previous section Alice and Bob were rejecting some particles which did not satisfy their needs).
\end{enumerate}

There is no one unique measure of entanglement. Let's now define conditions every ,,decent'' measure of entanglement $E$ must satisfy:
\begin{enumerate}[leftmargin=*,labelindent=1em,label=\textbf{(E\theenumi)}]
	\item we cannot produce entanglement from separable states, so
		\begin{equation}
			E(\hat\rho) = 0 \qquad \Longleftrightarrow \qquad \hat\rho \mathrm{\ is\ separable};
		\end{equation}
	\item entanglement cannot change under unitary operations corresponding to change of basis:
		\begin{equation}
			E(\hat\rho) = E(U_A\otimes U_B \hat\rho U_A^\dagger\otimes U_B^\dagger);
		\end{equation}
	\item expected entanglement cannot increase under LGM+CC+PS operations, as we demanded in postulate (\ref{post:fundamental_law}).
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Von Neumann entropy and entropy of entanglement}

\memph{Von Neumann entropy} is quantum generalisation of classical Boltzmann entropy and Shannon entropy known from information theory. It is given by formula
\begin{equation}
	S_{vN}(\hat\rho) = -\tr(\hat\rho\log_b\hat\rho)
\end{equation}
where we take $\log_b = \log_2 \equiv \lg$ for qubit, $\log_b = \log_3$ for qutrit, etc. If $\hat\rho$ is written in terms of its eigenvectors as
\begin{equation}
	\hat\rho = \sum\limits_i \lambda_i\ket{i}\bra{i},
\end{equation}
then the von Neumann entropy is
\begin{equation}
	S_{vN}(\hat\rho) = -\sum\limits_i \lambda_i \ln \lambda_i
\end{equation}
therefore \memph{for pure states} (only) it is always equal zero and can be used to describe amount of absence of knowledge (or ,,mixedness'') of state.

We defined entangled states as those which can not be described by convex combination of states of subsystems. That means entangled system must always be considered as a whole since states of its subsystems separated are not well defined. Roughly saying, some (or whole in case of maximally entangled state) information contained in a systems must be used to describe whole system, instead of its parts. We already saw this situation in example (\ref{ex:mix}). This in turn may be used to quantify amount of entanglement \emph{of pure state} $\hat P_{AB}$ as von Neumann entropy of states of its subsystems:
\begin{equation}
	E_{ent}(\hat P_{AB}) = -\tr(\hat\rho_A\log_b\hat\rho_A) = -\tr(\hat\rho_B\log_b\hat\rho_B)
\end{equation}
which we call \memph{entropy of entanglement}, where $\hat\rho_A = \tr_B \hat P_{AB}$ and $\hat\rho_B = \tr_A \hat P_{AB}$.

Entropy of entanglement has also one important feature. It can be shown that using distillation procedure we reviewed in previous section one can from $N$ weakly entangled states $\hat\rho$ produce $NE(\rho)$ maximally entangled Bell $\ket{\Phi^+}$ states. That is reason why it is generally believed that entropy of entanglement best entanglement measure for pure states and any measure defined for mixed states should reproduce entropy of entanglement on pure state limit.

