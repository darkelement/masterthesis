\section{Geometry of the set of states}

This section introduces notion of geometry of quantum states basing on Bloch vector. First case of two level systems is presented, then generalization to $n$-level system is provided and three-level systems are discussed in more details. This section is supported mainly by work done by Kimura in \cite{kimura_2003}.

\subsection{Qubit case}

Let's consider now the simplest possible quantum state --- qubit. Qubit represents a system that can be only in two states (e.g. spin: \{up, down\}, energy: \{ground, excited\}). Density matrices of qubits are therefore matrices $2\times2$. We have said that density matrices satisfy conditions [\ref{eq:density}]. Now we need a convenient way to represent them. Following four matrices constitute basis of space of self-adjoint matrices:
\begin{equation}
  \begin{split}
		\identity = \begin{pmatrix}1&0\\0&1\end{pmatrix}, &\quad
		\sigma_1 =  \begin{pmatrix}0&1\\1&0\end{pmatrix}, \\
		\sigma_2 =  \begin{pmatrix}0&-i\\i&0\end{pmatrix}, &\quad
		\sigma_3 =  \begin{pmatrix}1&0\\0&-1\end{pmatrix}.
	\end{split}
\end{equation}
We could use any other basis, but this one is specially convenient because of useful properties of Pauli matrices $\sigma_i$:
\begin{subequations}
	\begin{align}
		\sigma_i^\dagger &= \sigma_i, \\
		\tr\sigma_i &= 0, \\
		\tr\sigma_i\sigma_j &= 2\delta_{ij}, \\
		\commutator{\sigma_i}{\sigma_j} &= 2i\epsilon_{ijk}\sigma_k, \\
		\anticommutator{\sigma_i}{\sigma_j} &= 2\delta_{ij}\identity, \\
		(\vec\sigma\cdot\vec a)(\vec\sigma\cdot\vec b) &= \vec a\cdot\vec b\identity + i\vec\sigma\cdot(\vec a\times\vec b), \\
		\det\sigma_i &= -1.
	\end{align}
\end{subequations}
Using above basis every density matrix can be written as:
\begin{equation}
	\hat\rho = \hat\rho(\vec r) = \frac{1}{2}\left(\identity + \vec r\cdot\vec\sigma\right) =
	\frac{1}{2}\begin{pmatrix}
			1 + r_3   &  r_1 - ir_2 \\
		r_1 + ir_2  &  1 - r_3 \\
	\end{pmatrix}
\end{equation}

Any linear operator $\hat\rho \in \mathcal{E}^2$ can be uniquely characterized by a 3-dimensional real vector $\vec r=(r_1,r_2,r_3) \in \mathbbm{R}^3$, but only certain lengths (imposed by equation [\ref{eq:density:positivity}]) of this vector correspond to physical quantum states. That means we can imagine set of quantum states as a (convex) subset of $\mathbbm{R}^3$. Let's check how does it look like.
\begin{equation}
	\begin{split}
		\hat\rho^2 & = \frac{1}{4}\left(\identity + \vec r\cdot\vec\sigma\right)\left(\identity + \vec r\cdot\vec\sigma\right) =
		               \frac{1}{4}\left(\identity + (\vec r\cdot\vec\sigma)(\vec r\cdot\vec\sigma) + 2(\vec r\cdot\vec\sigma)\right) = \\
		           & = \frac{1}{2}\left(\frac{1+r^2}{2}\identity + \vec r\cdot\vec\sigma\right)
	\end{split}
\end{equation}
Using (\ref{eq:trace_square}) we find that every vector $\vec r$ representing quantum state must have length lower or equal 1
\begin{equation}
	\tr{\hat\rho^2} \leq 1 \quad\Longrightarrow\quad \frac{1+r^2}{2} \leq 1 \quad\Longrightarrow\quad r \leq 1,
\end{equation}
and, in particular in pure state case, its length is exactly 1
\begin{equation}
	\hat\rho^2 = \hat\rho \quad\Longrightarrow\quad \frac{1+r^2}{2} = 1 \quad\Longrightarrow\quad r = 1.
\end{equation}
That means one can imagine set of quantum states corresponding to two-level systems as a 3-dimensional ball
\begin{equation}
	\Omega_2 = \left\{\vec r \in\mathbbm{R}^3: \|\vec r\| \leq 1\right\} \equiv B^3,
\end{equation}
and set of pure states as a sphere
\begin{equation}
	\partial_e\Omega_2 = \partial\Omega_2 = \left\{\vec r \in\mathbbm{R}^3: \|\vec r\| = 1\right\} \equiv \partial B^3 \equiv S^2.
\end{equation}
Interior of the ball corresponds to mixed states.
We will later call this sphere the ,,Bloch sphere'' or the ,,Bloch surface''.

Knowing that Bloch surface is a sphere we can change coordinates:
\begin{equation}
	\vec r = \vec r(\phi, \theta) = (\sin\theta\cos\phi, \sin\theta\sin\phi, \cos\theta), \qquad 0 \leq \theta \leq \pi, \quad 0 \leq \phi < 2 \pi.
\end{equation}
Then density matrix corresponding to this \memph{pure} state is:
\begin{equation}
	\hat\rho_\mathrm{pure}(\phi,\theta) = \frac{1}{2}\begin{pmatrix}
		1+\cos\theta         &  e^{-i\phi}\sin\theta \\
		e^{i\phi}\sin\theta  &  1-\cos\theta \\
	\end{pmatrix} = \begin{pmatrix}
		\cos^2\frac\theta2                        & \sin\frac\theta2\cos\frac\theta2e^{-i\phi} \\
		\sin\frac\theta2\cos\frac\theta2e^{i\phi} & \sin^2\frac\theta2\\
	\end{pmatrix}
\end{equation}
This parametrisation will be later very useful.

Each pair of antipodal points correspond to mutually orthogonal state vectors. States $\ket0\bra0 = \begin{pmatrix}1&0\\0&0\end{pmatrix}$ and $\ket1\bra1 = \begin{pmatrix}0&0\\0&1\end{pmatrix}$ are usually depicted as north and south pool of sphere accordingly and are not represented uniquely.

Maximally mixed state, that is the one in center of Bloch ball is $\frac12\begin{pmatrix}1&0\\0&1\end{pmatrix}$. Indeed, its eigenvalues (probabilities of that particle is prepared in antipodal states) are $\left\{\frac12,\frac12\right\}$, what means we are ,,maximally uncertain'' of what pure state the particle is in.

\begin{figure}[hbt]
	\centering{
		\begin{tikzpicture}[
				scale=4,
				node distance=0.1,
				axis/.style={very thick, ->, >=stealth'},
				circle/.style={very thick},
				ellipse/.style={densely dashed},
				cast/.style={dotted},
				arc/.style={very thin},
				point/.style={color=black},
			]
			\path (0.0,-1.0) coordinate (P0);
			\path (0.0, 1.0) coordinate (P1);
			\path (0.3, 0.7) coordinate (P);
			\path (0.3,-0.1) coordinate (CXY);
		
			\draw[circle]  (0, 0) circle  (1.0);
			\draw[ellipse] (0, 0) ellipse (1.0 and 0.3);
			\draw[axis] (  0:0) -- (  0:1.2) node[right] {$y$};
			\draw[axis] ( 90:0) -- ( 90:1.2) node[right] {$z$};
			\draw[axis] (225:0) -- (225:0.7) node[below] {$x$};

			\fill[point] (P0) circle (0.03) node[below right=of P0] {$\ket0$};
			\fill[point] (P1) circle (0.03) node[below right=of P1] {$\ket1$};

			\fill[point] (P) circle (0.02) node[right=of P] {$\ket\psi$};
			\draw (0,0) -- (P);
			\draw[dotted] (CXY) -- (0,0);
			\draw[dotted] (CXY) -- (P);

			\draw[arc] (0,0) ++( 90:0.5) arc (90:67:0.5) node[midway,above]{$\vartheta$};
			\draw[arc] (0,0) ++(225:0.15) arc (260:305:0.5 and 0.1) node[midway,below]{$\varphi$};
			
		\end{tikzpicture}
	}
	\caption{Bloch surface for qubit.\label{fig:bloch_sphere}}
\end{figure}

In simplest possible quantum state, qubit, we have very simple and easy to imagine situation. In more complex systems things are rapidly getting more complicated. In later part of this section we will duscus most significant examples, namely thee- and four-level systems.

\subsection{N-level system}

Before we proceed to three- and four-level systems let's make some more general remarks. Pauli matrices are example of more general set of matrices $\hat\lambda$, that is they are generators of special unitary group of degree $n$, $SU(n)$, which are required to satisfy following relations:
\begin{subequations}
	\begin{align}
		\hat\lambda_i^\dagger &= \hat\lambda_i, \\
		\tr\hat\lambda_i &= 0, \\
		\tr\hat\lambda_i\hat\lambda_j &= 2\delta_{ij}.
	\end{align}
\end{subequations}
Moreover they constitute Lie algebra $su(n)$ characterized by structure constants $f_{ijk}$ (completely antisymmetric tensor) and $g_{ijk}$ (completely symmetric tensor):
\begin{subequations}
	\begin{align}
		\commutator{\hat\lambda_i}{\hat\lambda_j} &= 2if_{ijk}\hat\lambda_k, \\
		\anticommutator{\hat\lambda_i}{\hat\lambda_j} &= \frac{4}{N}\delta_{ij}\identity + 2g_{ijk}\hat\lambda_k.
	\end{align}
\end{subequations}
For $n = 2$ (Pauli matrices) structure constants are $f_{ijk} = \epsilon_{ijk}$ and $g_{ijk} = 0$.

It is convenient to keep length of Bloch vector always equal $r = 1$
\begin{equation}
	\begin{split}
		\hat\rho^2 & = \frac{1}{n^2}\left(\identity_n + \vec r\cdot\vec{\hat\lambda}\right)\left(\identity_n + \vec r\cdot\vec{\hat\lambda}\right) = \\
		           & = \frac{1}{n^2}\left(\identity_n + (\vec r\cdot\vec{\hat\lambda})(\vec r\cdot\vec{\hat\lambda}) + 2(\vec r\cdot\vec{\hat\lambda})\right)
	\end{split}
\end{equation}
\begin{equation}
	\begin{split}
		1 \geq \tr\hat\rho^2 & = \frac{1}{n^2}\tr\left(\identity_n + (r_i\hat\lambda_i)(r_j\hat\lambda_j)\right) =
		                         \frac{1}{n} + \frac{1}{n^2}r_ir_j\tr\left(\hat\lambda_i\hat\lambda_j\right) = \\
		                     & = \frac{1}{n} + \frac{2}{n^2}r_ir_j\delta_{ij} = \frac{1}{n} + \frac{2}{n^2}r^2
	\end{split}
\end{equation}
\begin{equation}
	r(n) \leq \sqrt{\frac{n}{2}(n-1)}
\end{equation}
Now, redefining parametrisation of density matrix to
\begin{equation}
	\hat\rho(\vec r) = \frac{1}{n}\left(\identity_n + \sqrt{\frac{n}{2}(n-1)}\vec r\cdot\vec{\hat\lambda}\right)
\end{equation}
we assure that Bloch vector is unit vector in any-level system.


\subsection{Three-level system}

Three-level systems (e.g. spin: \{-1,0,1\}) called also qutrits can be described in terms of vectors from Hilbert space, but to express them as density matrices we will use generators of $SU(3)$, so called Gell-Mann matrices:
\begin{equation}
	\begin{aligned}
		\hat\lambda_1 &= \begin{pmatrix}0&1&0\\1&0&0\\0&0&0\end{pmatrix}, &
		\hat\lambda_2 &= \begin{pmatrix}0&-i&0\\i&0&0\\0&0&0\end{pmatrix}, &
		\hat\lambda_3 &= \begin{pmatrix}1&0&0\\0&-1&0\\0&0&0\end{pmatrix}, \\
		\hat\lambda_4 &= \begin{pmatrix}0&0&1\\0&0&0\\1&0&0\end{pmatrix}, &
		\hat\lambda_5 &= \begin{pmatrix}0&0&-i\\0&0&0\\i&0&0\end{pmatrix}, &
		\hat\lambda_6 &= \begin{pmatrix}0&0&0\\0&0&1\\0&1&0\end{pmatrix}, \\
		\hat\lambda_7 &= \begin{pmatrix}0&0&0\\0&0&-i\\0&i&0\end{pmatrix}, &
		\hat\lambda_8 &= \frac{1}{\sqrt3}\begin{pmatrix}1&0&0\\0&1&0\\0&0&-2\end{pmatrix}.
	\end{aligned}
\end{equation}

Density matrix of qutrit is
\begin{equation}
	\hat\rho(\vec r) = \frac{1}{3}\left(\identity_3 + \sqrt3\vec{r}\cdot\vec{\hat\lambda}\right).
\end{equation}

Omitting detailed calculation (which can be found in Goyal et al. \cite{goyal_2011}) we can write:
\begin{equation}
	\hat\rho^2 = \frac{1}{9}\left(\identity+2\vec{r}\cdot\vec{r}+2\sqrt3\vec{r}\cdot\vec{\hat\lambda}+\sqrt3\vec{r}*\vec{r}\cdot\vec{\hat\lambda}\right),
\end{equation}
where
\begin{equation}
	(\vec{r}*\vec{r})_l = 3g_{jkl}r_jr_k.
\end{equation}
Then for pure states $\vec{r}\cdot\vec{r} = 1$, $\vec{r}*\vec{r} = \vec{r}$, so set of pure states is given by
\begin{equation}
	\partial_e\Omega_3 = \left\{\vec{r}\in\mathbbm{R}^8: \quad \vec{r}\cdot\vec{r} = 1, \quad \vec{r}*\vec{r} = \vec{r}\right\},
\end{equation}
and set of all physical quantum states is
\begin{equation}
	\Omega_3 = \left\{\vec{r}\in\mathbbm{R}^8: \quad \vec{r}\cdot\vec{r} \leq 1, \quad 3\vec{r}\cdot\vec{r} - \vec{r}*\vec{r}\cdot\vec{r} \leq 1\right\}.
\end{equation}

\begin{figure}[p]
	\centering{
		\subfloat[Triangle --- can be obtained by section in directions $\{\hat\lambda_1,\hat\lambda_2 \mathrm{\ or\ } \hat\lambda_3\}$ and $\{\hat\lambda_8\}$]{
			\label{fig:3l_2d_bloch:triangle}
			\includegraphics[width=0.45\textwidth]{images/3l_2d_bloch/triangle.eps}
		}
		\hspace{10pt}
		\subfloat[Parabola --- can be obtained by section in directions $\{\hat\lambda_4,\hat\lambda_5,\hat\lambda_6 \mathrm{\ or\ } \hat\lambda_7\}$ and $\{\hat\lambda_3\}$]{
			\label{fig:3l_2d_bloch:parabol}
			\includegraphics[width=0.45\textwidth]{images/3l_2d_bloch/parabol.eps}
		}
	}
	\centering{
		\subfloat[Ellipse --- can be obtained by section in directions $\{\hat\lambda_4,\hat\lambda_5,\hat\lambda_6 \mathrm{\ or\ } \hat\lambda_7\}$ and $\{\hat\lambda_8\}$]{
			\label{fig:3l_2d_bloch:elipse}
			\includegraphics[width=0.45\textwidth]{images/3l_2d_bloch/elipse.eps}
		}
		\hspace{10pt}
		\subfloat[Circle --- can be obtained by all other sections]{
			\label{fig:3l_2d_bloch:circle}
			\includegraphics[width=0.45\textwidth]{images/3l_2d_bloch/circle.eps}
		}
	}
	\caption{All possible two-dimensional sections of eight-dimensional Bloch set. Blue area marks points corresponding to quantum states. Red circle has radius equal $R_{red} = 1$, yellow --- $R_{yellow} = 1/\sqrt{3}$ and green --- $R_{green} = 1/2$. These images are created independently basing on work done in \cite{goyal_2011} and confirm results obtained there.\label{fig:3l_2d_bloch}}
\end{figure}

\begin{figure}[p]
	\centering{
		\subfloat[Obese tetrahedron --- obtained in base $\{\hat\lambda_3,\hat\lambda_4,\hat\lambda_5\}$]{
			\label{fig:3l_3d_bloch:obese_tetrahedron}
			\includegraphics[width=0.45\textwidth]{images/3l_3d_bloch/obese_tetrahedron.png}
		}
		\hspace{10pt}
		\subfloat[Praboloid --- obtained in base $\{\hat\lambda_1,\hat\lambda_4,\hat\lambda_6\}$]{
			\label{fig:3l_3d_bloch:paraboloid}
			\includegraphics[width=0.45\textwidth]{images/3l_3d_bloch/paraboloid.png}
		}
	}
	\centering{
		\subfloat[So called RS1 figure --- obtained in base $\{\hat\lambda_1,\hat\lambda_3,\hat\lambda_4\}$]{
			\label{fig:3l_3d_bloch:RS1}
			\includegraphics[width=0.45\textwidth]{images/3l_3d_bloch/RS1.png}
		}
		\hspace{10pt}
		\subfloat[So called RS2 figure --- obtained in base $\{\hat\lambda_1,\hat\lambda_4,\hat\lambda_8\}$]{
			\label{fig:3l_3d_bloch:RS2}
			\includegraphics[width=0.45\textwidth]{images/3l_3d_bloch/RS2.png}
		}
	}
	\caption{Three-dimensional sections of eight-dimensional Bloch set. Red color roughly corresponds to pure states, green fragments nearly touch ball of radius $1/2$, border of yellow and light green roughly corresponds to sphere of radius $1/\sqrt{3}$. These images are created independently basing on work done in \cite{goyal_2011} and confirm results obtained there.\label{fig:3l_3d_bloch}}
\end{figure}

In three-level systems, that is in one of most simplest cases, we obtained situation that is way more complicated than in two-level systems. Not only set of quantum states is now represented by eight-dimensional object, but it has also hard to imagine shape. Goyal et al. \cite{goyal_2011} distinguished four geometrically equivalent two-dimensional sections and seven geometrically equivalent three-dimensional sections. Figure (\ref{fig:3l_2d_bloch}) presents all possible shapes of two-dimensional sections, that is triangle, truncated parabola, ellipse and circle. Figure (\ref{fig:3l_3d_bloch}) in turn presents some examples of three-dimensional sections. These figures show how much more complicated geometry of quantum state is for three-level system. In higher-level systems complication grows. For more details on how these figures were drawn see section (\ref{sec:generating_sections}).

\subsection{Four-level systems\label{sec:single_systems_geometry:four_level_systems}}

Four-level systems may be viewed as one system that can be in four states or as two systems that can be in two states each. In this section we will discus the  first option and geometry of entangled states will be discussed in section (\ref{sec:composed_systems_geometry}).

To describe density matrices, as previously, we need basis composed of $4\times4$ self-adjoint, traceless matrices which generate all states. Let's choose:

\begin{equation}
	\begin{aligned}
		\hat\lambda_1    &= \begin{pmatrix} 0 & 1 & 0 & 0 \\ 1 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_2    &= \begin{pmatrix} 0 &-i & 0 & 0 \\ i & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_3    &= \begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 &-1 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, \\
		\hat\lambda_4    &= \begin{pmatrix} 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 0 \\ 1 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_5    &= \begin{pmatrix} 0 & 0 &-i & 0 \\ 0 & 0 & 0 & 0 \\ i & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_6    &= \begin{pmatrix} 0 & 0 & 0 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, \\
		\hat\lambda_7    &= \begin{pmatrix} 0 & 0 & 0 & 0 \\ 0 & 0 &-i & 0 \\ 0 & i & 0 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_8    &= \frac{1}{\sqrt3}
		                    \begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 &-2 & 0 \\ 0 & 0 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_9    &= \begin{pmatrix} 0 & 0 & 0 & 1 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ 1 & 0 & 0 & 0 \end{pmatrix}, \\
		\hat\lambda_{10} &= \begin{pmatrix} 0 & 0 & 0 &-i \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ i & 0 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_{11} &= \begin{pmatrix} 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 1 \\ 0 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \end{pmatrix}, & \quad
		\hat\lambda_{12} &= \begin{pmatrix} 0 & 0 & 0 & 0 \\ 0 & 0 & 0 &-i \\ 0 & 0 & 0 & 0 \\ 0 & i & 0 & 0 \end{pmatrix}, \\
		\hat\lambda_{13} &= \begin{pmatrix} 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 1 \\ 0 & 0 & 1 & 0 \end{pmatrix}, & \quad
		\hat\lambda_{14} &= \begin{pmatrix} 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 &-i \\ 0 & 0 & i & 0 \end{pmatrix}, & \quad
		\hat\lambda_{15} &= \frac{1}{\sqrt6}
		                    \begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 0 & 0 &-3 \end{pmatrix},
	\end{aligned}
	\label{eq:l4_matrices}
\end{equation}

Jakóbczyk and Siennicki \cite{jakobczyk_2001} distinguished eleven different two-dimensional shapes of sections which are presented on figure (\ref{fig:4l_2d_bloch}). Table (\ref{tab:2d_2q_sections}) summarises which type of shape corresponds to which section.

\begin{figure}[p]
	\centering{
		\subfloat[$A$]{
			\label{fig:4l_2d_bloch:A}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/A.eps}
		}
		\subfloat[$B$]{
			\label{fig:4l_2d_bloch:B}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/B.eps}
		}
		\subfloat[$C$]{
			\label{fig:4l_2d_bloch:C}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/C.eps}
		}
	}
	\centering{
		\subfloat[$D$]{
			\label{fig:4l_2d_bloch:D}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/D.eps}
		}
		\subfloat[$E$]{
			\label{fig:4l_2d_bloch:E}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/E.eps}
		}
		\subfloat[$F$]{
			\label{fig:4l_2d_bloch:F}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/F.eps}
		}
	}
	\centering{
		\subfloat[$G$]{
			\label{fig:4l_2d_bloch:G}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/G.eps}
		}
		\subfloat[$H$]{
			\label{fig:4l_2d_bloch:H}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/H.eps}
		}
		\subfloat[$I$]{
			\label{fig:4l_2d_bloch:I}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/I.eps}
		}
	}
	\centering{
		\subfloat[$J$]{
			\label{fig:4l_2d_bloch:J}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/J.eps}
		}
		\subfloat[$K$]{
			\label{fig:4l_2d_bloch:K}
			\includegraphics[width=0.28\textwidth]{images/4l_2d_bloch/K.eps}
		}
	}
	\caption{Two-dimensional sections of fifteen-dimensional Bloch set. These images are created independently basing on work done in \cite{jakobczyk_2001} and confirm results obtained there.\label{fig:4l_2d_bloch}}
\end{figure}

