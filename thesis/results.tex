\section{Results}

In this section we define some state families, then evaluate their entanglement depending on families parameter. Calculations are performed in case of two- and three-qubit states.

Values of entanglement evaluated with different measures fulfill some relations, for example Bures measure should always give bigger evaluation than Hilbert-Schmidt. For readability of plots entanglement was scaled so that it is equal one for maximally entangled states and these relations are not preserved on plots.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two qubits}

Here era given states used for calculation of entanglement in case of two qubits:

\begin{itemize}
	\item Werner state family:
			\begin{equation}
				\hat\rho_\mathrm{Werner} = F\ket{\Phi^+}\bra{\Phi^+} + \frac{1-F}{3}(\ket{\Psi^-}\bra{\Psi^-} + \ket{\Psi^+}\bra{\Psi^+} + \ket{\Phi^-}\bra{\Phi^-}).
			\end{equation}
			In language of density matrices it is:
			\begin{equation}
				\hat\rho_\mathrm{Werner} = F \begin{pmatrix}
						\frac12 & 0 & 0 & \frac12 \\
						    0   & 0 & 0 &     0   \\
						    0   & 0 & 0 &     0   \\
						\frac12 & 0 & 0 & \frac12 \\
					\end{pmatrix} + \frac{1-F}{3} \begin{pmatrix}
						 \frac12 & 0 & 0 & -\frac12 \\
						     0   & 1 & 0 &      0   \\
						     0   & 0 & 1 &      0   \\
						-\frac12 & 0 & 0 &  \frac12 \\
					\end{pmatrix}.
			\end{equation}
			Parameter $F$ is called \emph{fidelity}.

	\item Horodecki's state family:
			\begin{equation}
				\hat\rho_\mathrm{Horodecki}(q,a) = q\ket{\Psi_1}\bra{\Psi_1} + (1-q)\ket{\Psi_2}\bra{\Psi_2}
			\end{equation}
			where
			\begin{equation}
				\ket{\Psi_1} = a\ket{00} + \sqrt{1-a}\ket{11}
				\quad\mathrm{and}\quad
				\ket{\Psi_2} = a\ket{10} + \sqrt{1-a}\ket{01}.
			\end{equation}
			In language of density matrices it is:
			\begin{equation}
				\hat\rho_\textrm{Horodecki}(q,a) = \begin{pmatrix}
								qa^2       &          0         &          0         & qa\sqrt{1-a^2} \\
								 0         &    (1-q)(1-a^2)    & (1-q)a\sqrt{1-a^2} &        0       \\
								 0         & (1-q)a\sqrt{1-a^2} &      (1-q)a^2      &        0       \\
						qa\sqrt{1-a^2} &          0         &          0         &     q(1-a^2)   \\
					\end{pmatrix}
			\end{equation}
			To keep it dependent only from one parameter we will put $a=\frac34$ in later calculations.

	\item Chen state family:
			\begin{equation}
				\hat\rho_\mathrm{Chen} = \begin{pmatrix}
						\frac12(\lambda_+ + \lambda_-\cos\phi) &     0     &     0     &  \frac12\lambda_-\sin\phi\ e^{i\eta}   \\
						                  0                    & \lambda_1 &     0     &                   0                    \\
						                  0                    &     0     & \lambda_2 &                   0                    \\
						  \frac12\lambda_-\sin\phi\ e^{i\eta}  &     0     &     0     & \frac12(\lambda_+ + \lambda_-\cos\phi) \\
					\end{pmatrix}
			\end{equation}
			where $\lambda_+=\lambda_0 + \lambda_3$ and $\lambda_-=\lambda_0 - \lambda_3$. For our purposes we chose two one-parameter ,,subfamilies'':
			\begin{equation}
				\hat\rho_\mathrm{Chen1}(\lambda) = \begin{pmatrix}
							 \frac\lambda2 & 0 &     0     & -\frac\lambda2 \\
							        0      & 0 &     0     &        0       \\
							        0      & 0 & 1-\lambda &        0       \\
							-\frac\lambda2 & 0 &     0     &  \frac\lambda2 \\
						\end{pmatrix}
			\end{equation}
			and
			\begin{equation}
				\hat\rho_\mathrm{Chen2}(\lambda) = \begin{pmatrix}
							 \frac\lambda2 &         0          &         0          & -\frac\lambda2 \\
							        0      & \frac15(1-\lambda) &         0          &        0       \\
							        0      &         0          & \frac45(1-\lambda) &        0       \\
							-\frac\lambda2 &         0          &         0          &  \frac\lambda2 \\
						\end{pmatrix}
			\end{equation}
\end{itemize}

Results are presented on figures (\ref{fig:result_werner}-\ref{fig:result_chen2}). Parts (a) of figures present entanglement of state family depending on families parameter. Parts (b) present visualisation of two-dimensional slice through set of quantum states containing given family and maximally mixed state. Blue areas correspond to sets of entangled states, while gray areas correspond to sets of separable states. Orange lines represent family state. 

\begin{figure}[hpt]
	\centering\subfloat[Entanglement]{
		\includegraphics[width=\linewidth]{{images/results_2d/Werner.entanglement}.eps}
		\label{fig:result_werner:entanglement}
	}
	\newline
	\centering\subfloat[Visualisation: blue --- entangled states, gray --- separable states, orange --- Werner state family]{
		\includegraphics[width=0.5\linewidth]{{images/families/Werner.vis}.eps}
		\label{fig:result_werner:vis}
	}
	\caption{Entanglement of Werner state family\label{fig:result_werner}}
\end{figure}

\begin{figure}[hpt]
	\centering\subfloat[Entanglement]{
		\includegraphics[width=\linewidth]{{images/results_2d/Horodecki.entanglement}.eps}
		\label{fig:result_horodecki:entanglement}
	}
	\newline
	\centering\subfloat[Visualisation: blue --- entangled states, gray --- separable states, orange --- Horodecki's state family]{
		\includegraphics[width=0.5\linewidth]{{images/families/Horodecki.vis}.eps}
		\label{fig:result_horodecki:vis}
	}
	\caption{Entanglement of Horodecki's state family\label{fig:result_horodecki}}
\end{figure}

\begin{figure}[hpt]
	\centering\subfloat[Entanglement]{
		\includegraphics[width=\linewidth]{{images/results_2d/Chen1.entanglement}.eps}
		\label{fig:result_Chen1:entanglement}
	}
	\newline
	\centering\subfloat[Visualisation: blue --- entangled states, gray --- separable states, orange --- Chen1 state family]{
		\includegraphics[width=0.5\linewidth]{{images/families/Chen1.vis}.eps}
		\label{fig:result_chen1:vis}
	}
	\caption{Entanglement of Chen1 state family\label{fig:result_chen1}}
\end{figure}

\begin{figure}[hpt]
	\centering\subfloat[Entanglement]{
		\includegraphics[width=\linewidth]{{images/results_2d/Chen2.entanglement}.eps}
		\label{fig:result_chen2:entanglement}
	}
	\newline
	\centering\subfloat[Visualisation: blue --- entangled states, gray --- separable states, orange --- Chen2 state family]{
		\includegraphics[width=0.5\linewidth]{{images/families/Chen2.vis}.eps}
		\label{fig:result_chen2:vis}
	}
	\caption{Entanglement of Chen2 state family\label{fig:result_chen2}}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three qubits}

States used for calculations of entanglement in case of three qubits are given below:

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_0(p)
		& = p
		\left( \ket{000}+\ket{111} \right)
		\left( \bra{000}+\bra{111} \right)
		\\ & + (1-p)
		\left( \ket{100}+\ket{010}+\ket{001} \right)
		\left( \bra{100}+\bra{010}+\bra{001} \right)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_1(p)
		& = p
		\left( \ket{000}+\ket{111} \right)
		\left( \bra{000}+\bra{111} \right)
		\\ & + (1-p)
		\left( \ket{010}+\ket{100} \right)
		\left( \bra{010}+\bra{100} \right)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_2(p)
		& = p
		\left( \ket{000}+\ket{111} \right)
		\left( \bra{000}+\bra{111} \right)
		\\ & + (1-p)
		\ket{010}
		\bra{010}
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_3(p)
		& = p
		\ket{000}
		\bra{000}
		+ (1-p)
		\ket{111}
		\bra{111}
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_4(p)
		& = p
		\left( \ket{000}+\ket{111} \right)
		\left( \bra{000}+\bra{111} \right)
		\\ & + (1-p)
		\left( \ket{000}-\ket{111} \right)
		\left( \bra{000}-\bra{111} \right)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_5(p)
		& = p
		\left( \ket{000}+\ket{111} \right)
		\left( \bra{000}+\bra{111} \right)
		\\ & + (1-p)
		\left( \ket{001}+\ket{110} \right)
		\left( \bra{001}+\bra{110} \right)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_6(p)
		& = p
		\ket{001}
		\bra{001}
		+ (1-p)
		\left( \ket{100}+\ket{010} \right)
		\left( \bra{100}+\bra{010} \right)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_7(p)
		& = p
		\ket{001}
		\bra{001}
		+ (1-p)
		\left( \ket{100}+\frac12\ket{010} \right)
		\left( \bra{100}+\frac12\bra{010} \right)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\hat\rho^{2,2,2}_8(p)
		& = p
		\ket{001}
		\bra{001}
		+ (1-p)
		\left( \frac12\ket{100}+\ket{010} \right)
		\left( \frac12\bra{100}+\bra{010} \right)
	\end{split}
\end{equation}

Results are presented on figures (\ref{fig:result_3d0})-(\ref{fig:result_3d8}). Compared to case with two qubits we had to find minimum of 447-argument function for three qubits (instead of just 79-argument function) calculations would take therefore very long time, so in this case lower number of points was taken into account.

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d0.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_0$ state family\label{fig:result_3d0}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d1.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_1$ state family\label{fig:result_3d1}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d2.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_2$ state family\label{fig:result_3d2}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d3.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_3$ state family\label{fig:result_3d3}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d4.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_4$ state family\label{fig:result_3d4}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d5.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_5$ state family\label{fig:result_3d5}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d6.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_6$ state family\label{fig:result_3d6}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d7.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_7$ state family\label{fig:result_3d7}}
\end{figure}

\begin{figure}[hpt]
	\centering\includegraphics[width=\linewidth]{{images/results_3d/3d8.entanglement}.eps}
	\caption{Entanglement of $\hat\rho^{2,2,2}_8$ state family\label{fig:result_3d8}}
\end{figure}

