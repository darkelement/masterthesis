Calculating logarithm of matrix is very common procedure in quantum information science. This appendix will present a numerical method for calculating it.

Logarithm of matrix is defined analogically to logarithm over complex plane. We say matrix $B$ is logarithm of matrix $A$ if
\begin{equation}
	e^{B} = A.
\end{equation}
Here also holds expression for Taylor series of logarithm:
\begin{equation}
	\log{\left(A+\identity\right)} = A - \frac{A^2}{2} + \frac{A^3}{3} - \frac{A^4}{4} + \cdots
\end{equation}

If $A$ is diagonalisable our task is simple --- we have to diagonalize it:
\begin{equation}
	D = V^{-1}AV.
\end{equation}
Then:
\begin{equation}
	\begin{split}
		\log{A}
		   &= \log{\left(VDV^{-1}\right)}
		=\\&= \left(VDV^{-1}-\identity\right) - \frac{\left(VDV^{-1}-\identity\right)^2}{2} + \frac{\left(VDV^{-1}-\identity\right)^3}{3} - \cdots
		=\\&= \left(VDV^{-1}-VV^{-1}\right) - \frac{\left(VDV^{-1}-VV^{-1}\right)^2}{2} + \frac{\left(VDV^{-1}-VV^{-1}\right)^3}{3} - \cdots
		=\\&= \left(V(D-\identity)V^{-1}\right) - \frac{\left(V(D-\identity)V^{-1}\right)^2}{2} + \frac{\left(V(D-\identity)V^{-1}\right)^3}{3} - \cdots
		=\\&= V\left(\left(D-\identity\right) - \frac{\left(D-\identity\right)^2}{2} + \frac{\left(D-\identity\right)^3}{3} - \cdots\right)V^{-1}
		=\\&= V\log{\left(D\right)}V^{-1}
	\end{split}
\end{equation}
Logarithm of diagonal matrix $D$ is simply matrix of logarithms of diagonal elements of D:
\begin{equation}
	\log D = \log
		\begin{pmatrix}
			  d_1  &    0   & \cdots &    0   \\
			   0   &   d_2  & \cdots &    0   \\
			\vdots & \vdots & \ddots & \vdots \\
			   0   &    0   & \cdots &  d_n   \\
		\end{pmatrix} = 
		\begin{pmatrix}
			\log{(d_1)} &       0     & \cdots &       0     \\
			      0     & \log{(d_2)} & \cdots &       0     \\
			   \vdots   &    \vdots   & \ddots &    \vdots   \\
			      0     &       0     & \cdots & \log{(d_n)} \\
		\end{pmatrix}.
	\label{eq:log_diagonal} 
\end{equation}

If a matrix can not be diagonalized it can be represented in Jordan form. Jordan matrix is block-diagonal matrix
\begin{equation}
	J = \begin{pmatrix}
		  J_1  &    0   & \cdots &    0   \\
		   0   &   J_2  & \cdots &    0   \\
		\vdots & \vdots & \ddots & \vdots \\
		   0   &    0   & \cdots &  J_n   \\	
	\end{pmatrix}
\end{equation}
where
\begin{equation}
	J_i = \begin{pmatrix}
		\lambda_i &     1     &    0      & \cdots &     0     &     0     \\
		    0     & \lambda_i &    1      & \cdots &     0     &     0     \\
		    0     &     0     & \lambda_i & \ddots &     0     &     0     \\
		 \vdots   &  \vdots   &  \vdots   & \ddots &  \ddots   &  \vdots   \\
		    0     &     0     &    0      & \cdots & \lambda_i &     1     \\	
		    0     &     0     &    0      & \cdots &     0     & \lambda_i \\	
	\end{pmatrix}
\end{equation}
and $\lambda_i$'s are eigenvalues of $J$. $A$ can be now rewritten as:
\begin{equation}
	A = V\log{\left(J\right)}V^{-1}
\end{equation}
Logarithm of $J$ can be symbolically written in similar form as in (\ref{eq:log_diagonal}) by putting blocks obtained by taking logarithms of $J_i$'s on diagonal:
\begin{equation}
	\log J = 
		\begin{pmatrix}
			\log{(J_1)} &       0     & \cdots &       0     \\
			      0     & \log{(J_2)} & \cdots &       0     \\
			   \vdots   &    \vdots   & \ddots &    \vdots   \\
			      0     &       0     & \cdots & \log{(J_n)} \\
		\end{pmatrix}.
\end{equation}
Now we only have to calculate logarithm of $J_i$. It can be rewritten in form:
\begin{equation}
	J_i = \lambda_i \begin{pmatrix}
		   1   & \lambda_i^{-1} &        1       & \cdots &    0   &        0       \\
		   0   &        1       & \lambda_i^{-1} & \cdots &    0   &        0       \\
		   0   &        0       &        1       & \ddots &    0   &        0       \\
		\vdots &     \vdots     &     \vdots     & \ddots & \ddots &     \vdots     \\
		   0   &        0       &        0       & \cdots &    1   & \lambda_i^{-1} \\
		   0   &        0       &        0       & \cdots &    0   &         1      \\
	\end{pmatrix}
	=
	\lambda_i\left(\identity+K\right).
\end{equation}
Now:
\begin{equation}
	\begin{split}
		\log{J_i}
		  &= \log{\left(\lambda_1\left(\identity+K\right)\right)}
		   = \log(\lambda_1)\identity + \log{\left(\identity+K\right)}
		\\&= \log(\lambda_1)\identity + K - \frac{K^2}{2} + \frac{K^3}{3} - \frac{K^4}{4} + \cdots,
	\end{split}
\end{equation}
and since matrix $K$ is nilpotent, the sum in last equation has finite number of elements. For calculating $\log(J)$ it is required to $\lambda_i \neq 0$, or equivalently $\det(J) \neq 0$.


