\section{Mixed states}

Pure states are great way to describe physical systems theoretically, but in real life it is often hard (or even impossible) to obtain complete information about system (e.g. due to decoherence). That is why physicists introduce \memph{mixed states}, that is, roughly saying, states that do \memph{not} provide complete description of system. Mixed states correspond to probabilistic mixture (statistical ensemble) of pure states, that is pairs of pure state $\ket{\psi_i}$ and probability $p_i$ of its occurrence:
\begin{equation}
	\mathrm{mixed\ state} \equiv \left\{\left(p_i,\ket{\psi_i}\right)\right\}_i.
\end{equation}

Now we need to find easy way to describe both mixed and pure states in the same (mathematical) language. Since mixed state is statistical ensemble of pure states, so:
\begin{equation}
	\langle\A\rangle_{\textrm{mixed}} = \sum\limits_ip_i\langle\A\rangle_i, \qquad \sum\limits_ip_i = 1
	\label{eq:mixed_probabilities}
\end{equation}
Then:
\begin{equation}
	\langle\A\rangle_{\textrm{mixed}} = \sum\limits_ip_i\tr(\hat\rho_i\A) = \tr\left(\sum\limits_ip_i\hat\rho_i\A\right) = \tr(\hat\rho\A)
\end{equation}
Where $\hat\rho_i$ corresponds to $i$-th pure state and $\hat\rho=\sum_ip_i\hat\rho_i$.

The above means we can describe quantum mixed state with means of matrix $\hat\rho$, just like we did it in case of pure states.

Properties of matrix $\hat\rho$ for mixed state are:
\begin{equation}
  \tr\hat\rho = 1, \qquad \hat\rho = \hat\rho^\dagger, \qquad \hat\rho \geq 0.
	\label{eq:density_matrix:mixed_properties}
\end{equation}
This time not only $\hat\rho = \hat\rho^2$, but also $\hat\rho \geq 0$ (eigenvalues are not only $0$ or $1$ but any positive). It is worth to note that in qubit case properties (\ref{eq:density_matrix:mixed_properties}) are equivalent to
\begin{equation}
  \tr\hat\rho = 1, \qquad \hat\rho = \hat\rho^\dagger, \qquad \tr\hat\rho^2 \leq 1.
	\label{eq:trace_square}
\end{equation}
For higher dimensions these are only necessary conditions for (\ref{eq:density_matrix:mixed_properties}). Because matrix $\rho$ is self adjoint, probabilities $p_i$ from equation (\ref{eq:mixed_probabilities}) are its eigenvalues.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Density matrices}

Now we can reverse our reasoning and postulate that 
\begin{postulate}
	Any operator $\hat\rho$ satisfying following conditions corresponds to a quantum state:
	\begin{subequations}
		\label{eq:density}
		\begin{align}
			\tr\hat\rho & = 1,             \label{eq:density:trace_one}\\
			\hat\rho^\dagger & = \hat\rho, \label{eq:density:selfadjointment}\\
			\hat\rho & \geq 0.             \label{eq:density:positivity}
		\end{align}
	\end{subequations}
	Operators satisfying additionally
	\begin{equation}
		\hat\rho^2 = \hat\rho
	\end{equation}
	correspond to pure states.
\end{postulate}

\begin{definition}[Density matrix]
	Any matrix satisfying conditions (\ref{eq:density}) is called \memph{density matrix} (or \memph{state operator}).
\end{definition}
Let's denote set of all density matrices (quantum states) as
\begin{equation}
	\States \eqdef \left\{\hat\rho:\ \tr\hat\rho = 1,\ \hat\rho^\dagger = \rho,\ \hat\rho \geq 0 \right\},
\end{equation}
and set of all pure states as
\begin{equation}
	\PureStates \eqdef \left\{\hat\rho:\ \tr\hat\rho = 1,\ \hat\rho^\dagger = \rho,\ \hat\rho^2 = \hat\rho \right\}.
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Mathematical representation}

As we have said, mixed states are probabilistic mixtures of pure states. That makes convex set (see appendix \ref{ch:convex_sets}) a perfect mathematical object to describe them:
\begin{postulate}
	Set of pure states is set of extreme points of all physical states.
\end{postulate}

Let's check if our definitions agree with this postulate. We have to prove two things: 
\begin{itemize}
	\item $\conv\PureStates \subset \States$, i.e. every convex combination of pure states has properties of mixed states. Indeed, $\hat\rho^\dagger = \hat\rho$ and $\hat\rho > 0$ are true due to linearity of inner product, and
			\begin{equation}
				\tr\hat\rho = \tr\sum\limits_ip_i\hat\rho_i = \sum\limits_ip_i\tr\hat\rho_i = \sum\limits_ip_i = 1.
			\end{equation}
	\item $\States \subset \conv\PureStates$, i.e. every mixed state must be expressed by convex combination of pure states (there are no such mixed states that cannot be expressed by convex combination of pure states).

			Since matrices representing pure states are orthogonal projectors, we can choose such matrices that constitute basis in $\States$, therefore any state can be decomposed as follows:
			\begin{equation}
				\hat\rho = \sum\limits_ia_i\hat\rho_i.
			\end{equation}
			All we have to do now is to show that this combination is convex. Sum of coefficients is 1 because
			\begin{equation}
				1 = \tr\hat\rho = \tr\sum\limits_ia_i\hat\rho_i = \sum\limits_ia_i\tr\hat\rho_i = \sum\limits_ia_i.
			\end{equation}
			Positivity of $\hat\rho$ means $\innerp{\psi}{\hat\rho\psi} > 0$ for any vector $\ket\psi$. It is enough to check this only for basis vectors. Without loosing generality we can choose basis composed from eigenvectors with eigenvalue 1 of $\hat\rho_i$'s\footnote{Eigenspace corresponding to eigenvalue 1 of any density matrix is one-dimensional because $\tr\hat\rho = 1$ and only possible eigenvalues are 0 or 1.}:
			\begin{equation}
				0 < \innerp{\phi_i}{\hat\rho\phi_i} = \innerp{\phi_i}{\sum\limits_ka_k\hat\rho_k\phi_i} = a_i.
			\end{equation}
			\begin{flushright}
				$\Box$
			\end{flushright}
\end{itemize}

