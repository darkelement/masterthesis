\section{Issues, ideas, conclusions}

\subsection{Enhancing algorithms speed}

We argued that genetic algorithm is best choice for our problem, it is thought quite slow compared to other methods. It is good idea to seek for some improvements to genetic algorithm from perspective of optimisation of entanglement. As genetic algorithm searches solution in separated steps, it is possible to monitor fitness of solutions. In this section we will apply some concepts which may seem to bring a speed-up and verify them by qualitative look at plots of entanglement of the best solution in population versus generation.

Figure (\ref{fig:evolution:few}) presents evolution of entanglement calculated with entropy of entanglement when optimising for seven states from Werner state family. Figure (\ref{fig:evolution:many}) presents evolution of states from Horodecki's state family when optimising Hilbert-Schmidt metric. Both simulations were stopped after 200 generations. Figure (\ref{fig:evolution:comparison}) presents evolution of Werner state with some modifications of used algorithm (differences are better visible on semi-logarithmic plot). Plot is averaged over 50 simulations, so it provides good statistical insight into given modification's speed. In this section we are going to discuss some possible modifications and try to find best one.

\begin{figure}
	\centering{
		\subfloat[Evolution of 7 chosen Werner states using entropy of entanglement along 200 generations.]{
			\label{fig:evolution:few}
			\includegraphics[width=\linewidth]{{images/evolution/Werner.entropy.few.evolution}.eps}
		}
		\newline
		\subfloat[Evolution of Horodecki state using Hilbert-Schmidt metric along 200 generations.]{
			\label{fig:evolution:many}
			\includegraphics[width=\linewidth]{{images/results_2d/Horodecki.Hilbert-Schmidt.result.evolution}.eps}
		}
	}
	\caption{Examples of evolution of best solution for choosen states.}
\end{figure}

\begin{figure}
	\centering{
		\subfloat[Linear scale]{
			\label{fig:evolution:comparison:simple}
			\includegraphics[width=\linewidth]{{images/evolution/evolution.comparison}.eps}
		}
		\newline
		\subfloat[Semi-logarithmic scale]{
			\label{fig:evolution:comparison:log}
			\includegraphics[width=\linewidth]{{images/evolution/evolution.comparison.log}.eps}
		}
	}
	\caption{Comparison of evolution in case of Werner state averaged over 50 simulations using different modifications to genetic algorithm.\label{fig:evolution:comparison}}
\end{figure}

\paragraph{Sharing best individuals.} While calculating a measure of entanglement we usually prepare a plot with lot of points close to each other. That means the density matrices being optimal solutions for close points may be very similar. During calculations it may happen that there was found very good solutions for one point, which is also a good one for neighbour, maybe even better than any known solution for this point at this time. The key concept of this modification is to share best solutions between neighbouring points to avoid redundant work.

Biological intuition for this approach may be as follows: let's imagine a chain of niches (e.g. valleys separated by mountains) with slightly different existential conditions. Evolution takes place independently in each niche but from time to time one individual with phenotype very well fitted for its niche migrate to neighbouring one. If it does not fit to conditions in this niche or there are many better fitted individuals, its genotype is simply doomed and do not contribute to genome pool of this niche. On the other hand, when the old population show lower fitness, the new-comer will have high chances to reproduce and improve overall fitness of whole population.

In figure (\ref{fig:evolution:many}) reader can find a cascade-like patterns which illustrate propagation of the ,,good'' genes caused by sharing best individuals. On figure (\ref{fig:evolution:comparison}) situation with and without sharing is represented by lines labeled ,,normal'' and ,,do not share best state'' accordingly. We can see that this approach shows advantage, especially in the middle of process, when individuals are not yet very specialized for conditions in their niches.

\paragraph{Frequency of mutations.}
In section (\ref{sec:genetic_algorithms}) about genetic algorithms we said that for situation in which we store genotype as an array of real numbers the probability of mutations should be high.
How high is good in our case? Lines labeled ,,normal'' and ,,mutate whole population'' in figure (\ref{fig:evolution:comparison}) correspond to modifications where we mutate 50\% and 100\% of population accordingly.
In first part of process, the modification with lower probability wins.
Indeed, with lower amount of mutations there is lower probability to disturb positive input from selection.
In the later part however, the second modification takes over.
That is because best individuals are very close to the ,,optimum'' in this part and only very subtle changes to genotype are required to increase fitness.
There is rather small probability to gain this changes in process of selection because there are no proper genes in gene pool and they must be introduced via mutations.
The approach with lower probability of mutation would do better if population was bigger (and therefore genetic diversity wider).

\paragraph{Random pure separable states.}
Because optimal state lies in surface of separable states it is profitable to favour somehow extreme points of set of separable states. The key concept of this approach is to initialize first population not with completely random states, but with random states containing genes for extreme points of set of separable states and by throwing-in these genes randomly during mutation. Those genes are easy to generate just by setting adequate parameters of density matrix to zeros or multiples of $\pi$. Figure (\ref{fig:evolution:comparison}) shows that this modification (label ,,random pure states'') starts very well, but it is getting slower over time. That is because even if process begins with states that are best to construct other states from them via selection, the genetic diversity is low. This modification therefore unfortunately does not give much speed-up to algorithm.

\paragraph{Evolution parameter.}
Simulation begins with set of random separable states, which are placed relatively far from each other. At this stage it is beneficial to let the states mutate quickly to make them converge to best solution fast. When whole population is close to solution we want them to mutate slowly (at this point big mutations will disturb results) so that they do not ,,overshoot'' their target. To achieve this an evolution parameter that is used by algorithm should be changed over time. In this thesis very basic approach is chosen. Mutation changes states by small amount taken from some distribution. Every time better solution is not found, the variance of this distribution is decreased. This approach ensures evolution does not slow down too slowly or too rapidly. In figure (\ref{fig:evolution:comparison}) line labeled ,,no evolution parameter'' corresponds to situation when evolution parameter is constant and line labeled ,,normal'' to situation described above. One can see that introduction of evolution parameter enormously speeds-up execution of the algorithm.

\subsection{Conclusions}

Things done in this master thesis include:
\begin{itemize}
	\item Creating software for calculating entanglement using predefined entanglement measures for predefined states.
		Calculation of entanglement required minimization of functions in form (\ref{eq:measure_distance}) what was based on genetic algorithm, but approach is generic and any kind of optimizing algorithm could be used.
	\item Creating software for visualization of two- and three-dimensional sections of Bloch set as well as set of separable states.
		Here only figures presenting sections of qutrit and pair of qubits ware presented, but, as above, approach is generic and generating sections for more complicated systems requires only defining corresponding density matrices.
\end{itemize}

Plots of entanglement for Werner and Horodecki states using relative entropy and concurrence were firstly given by Ramos and Souza in \cite{ramos_2003,ramos_souza_2002} and Vedral and Plenio in \cite{vedral_plenio_1998,plenio_vedral_1998}.
The results obtained in this thesis agree with original ones.
Other results, especially sections of set of separable states and entanglement of multipartite systems are published first time as far as I know.

In future work it would be interesting to try other than genetic algorithm method of finding minimum of distance measure, since genetic algorithm have many advantages described above, but they are also relatively slow.

Multipartite entanglement is much richer than bipartite, since there one can take into account separability of whole system (full separability) or of its subsystems (partial separability).
In this work only full separability was taken into consideration.
Also it would be interesting to develop ways to evaluate entanglement measures of forms different than (\ref{eq:measure_distance}), what is especially interesting in multipartite case since for example squashed entanglement (which, according to \cite{horodecki_2007}, is the only known measure that preserves monogamy\footnote{monogamy is property of entanglement which can be expressed as follows: ,,If two qubits A and B are maximally quantumly correlated they cannot be correlated at all with third qubit C'' \cite{horodecki_2007}}) does not have this form.


