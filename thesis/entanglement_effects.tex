\section{Non-classical effects}

In this section we discuss some effects related to properties of quantum theory and entanglement.

\subsection{No-cloning theorem}

No-cloning theorem or law of information conservation stands that there exists no physical process that would allow to copy information about quantum state from one system to another. In other words there is no operator $\C$ such that:
\begin{equation}
	\label{eq:clone_operator}
	\C\ket{\psi,\phi} = \ket{\psi,\psi}.
\end{equation}

Indeed, using (\ref{eq:clone_operator}) we obtain:
\begin{equation}
	\label{eq:clone_property}
	\begin{split}
		\C\left(\frac{1}{\sqrt2}(\ket{a_1} + \ket{a_2})\ket{b}\right) & = 
			\left(\frac{1}{\sqrt{2}}(\ket{a_1} + \ket{a_2})\right)\left(\frac{1}{\sqrt{2}}(\ket{a_1} + \ket{a_2})\right) = \\
		&= \frac{1}{2}\left(\ket{a_1, a_1} + \ket{a_1, a_2} + \ket{a_2, a_1} + \ket{a_2, a_2}\right),
	\end{split}
\end{equation}
but on the other hand every operator has to be linear
\begin{equation}
	\label{eq:clone_linearity}
	\begin{split}
		\C\left(\frac{1}{\sqrt{2}}(\ket{a_1} + \ket{a_2})\ket{b}\right) & =
		\frac{1}{\sqrt{2}}\C\ket{a_1, b} + \frac{1}{\sqrt{2}}\C\ket{a_2, b} = \\
	& = \frac{1}{\sqrt{2}}\ket{a_1, a_1} + \frac{1}{\sqrt{2}}\ket{a_2, a_2}.
	\end{split}
\end{equation}

(\ref{eq:clone_property}) and (\ref{eq:clone_linearity}) are obviously different, therefore operator $\C$ can not exist. We may look at this from two sides: either state $\ket\psi$ can not be ,,cloned'' from system $A$ to $B$ or information contained in system $B$ can not be ,,erased''.

\subsection{Teleportation}

Suppose experimenter from laboratory $A$, say Alice, wants to communicate to experimenter from laboratory $B$, Bob, an \memph{unknown} quantum bit $\ket{q} = a\ket0 + b\ket1$. Of course she can not perform measurement on her qubit because it will ,,diffuse'' information contained in it over whole her laboratory in process called \memph{decoherence}, but there is another way. Let's suppose Alice and Bob share maximally entangled state $\ket{\Phi^+}$ and they can communicate via classical means (e.g. by phone). Initial tripartite state is given by:
\begin{equation}
 	\ket\Xi_{A'AB} = \ket{q}_{A'} \otimes \ket{\Phi^+}_{AB} = (a\ket0_{A'} + b\ket1_{A'}) \otimes \frac{1}{\sqrt{2}}(\ket{00}+\ket{11})_{AB},	
\end{equation}
where $A'$ corresponds to Alice's unknown qubit, $A$ to Alice's part of entangled system and $B$ to Bobs part. This state can be written using the Bell basis on the system $A'A$ in the
following way:
\begin{equation}
	\begin{split}
		\ket\Xi_{A'AB} = \frac{1}{2}(
			  & \ket{\Phi^+}_{A'A}(a\ket0_B + b\ket1_B) + \\
			+ & \ket{\Phi^-}_{A'A}(a\ket0_B - b\ket1_B) + \\
			+ & \ket{\Psi^+}_{A'A}(a\ket1_B + b\ket0_B) + \\
			+ & \ket{\Psi^-}_{A'A}(a\ket1_B - b\ket0_B)).
	\end{split}
\end{equation}
Now Alice performs measurement on her two particles in Bell basis and informs Bob about results. All Bob has to do now is to rotate his particle (or laboratory) according to information he received from Alice. After this rotation Bob obtains state $\ket{q}$.

Some remarks need to be made:
\begin{itemize}
	\item All operations are \memph{local} i.e. no operation was performed on Alice's and Bobs particles at once --- Alice performed measurement on her two particles and Bob rotated his one.
	\item There is no transfer of matter nor energy, all that is teleported is information about state.
	\item There is no contradiction to no-cloning theorem --- state of particle $A'$ becomes undefined.
	\item Information does not move faster than light --- Alice and Bob had to use \memph{classical communication} channel to send information about result of measurement.
	\item Entanglement between Alice and Bob is destroyed, therefore they can not sent another qubit using the same pair of particles.
\end{itemize}

Pair shared by Alice and Bob was maximally entangled. In fact if they used pair possessing lower ,,amount'' of entanglement teleportation would not succeed. Moreover they could not produce entangled pair just by means of local operations, if they could, they would be able transfer quantum information without sharing a pair entangled in advance. That leads us to following postulate\footnote{still not proved}:
\begin{postulate}[Fundamental law of quantum information processing]
	\label{post:fundamental_law}
	By local operations and classical communication alone, Alice and Bob cannot increase the total amount of entanglement which they share.
\end{postulate}

\subsection{Entanglement distillation}

The above statement constitutes serious problem in real-life communication, where due to decoherence it is very hard to keep particles entangled. Fortunately methods allowing to transfer entanglement from large number of weakly entangled to small number of strongly entangled particles (called entanglement purification, distillation or concentration) were invented. Here we present one of possible algorithms first given by Deutsch et al. in \cite{deutsch_1996}.

First Alice and Bob must share two pairs of partially entangled pairs. In second step Alice rotates her particles with operation:
\begin{equation}
	\ket0 \rightarrow \frac{1}{\sqrt2}(\ket0 - i\ket1),
	\qquad
	\ket1 \rightarrow \frac{1}{\sqrt2}(\ket1 - i\ket0).
\end{equation}
Bob performs reverse operation:
\begin{equation}
	\ket0 \rightarrow \frac{1}{\sqrt2}(\ket0 + i\ket1),
	\qquad
	\ket1 \rightarrow \frac{1}{\sqrt2}(\ket1 + i\ket0).
\end{equation}
Then both Alice and Bob perform CNOT (controlled NOT) operation on qubits they hold, which has effect:
\begin{equation}
	\begin{split}
		\ket{00} \rightarrow \ket{00}, \qquad
		\ket{01} \rightarrow \ket{01}, \\
		\ket{10} \rightarrow \ket{11}, \qquad
		\ket{11} \rightarrow \ket{10}. \\
	\end{split}
\end{equation}
First qubit is called ,,control'', and second ,,target''. Now Alice and Bob perform measurement on their target qubits, consult results and if they coincide they keep their control qubits (only). Otherwise they reject all four particles.

Of course whole algorithm may be repeated on output pairs. One can show that after each repetition
\begin{equation}
	\begin{split}
		\tilde{A} = \frac{A^2 + B^2}{N}, & \qquad
		\tilde{B} = \frac{2CD}{N}, \\
		\tilde{C} = \frac{C^2 + D^2}{N}, & \qquad
		\tilde{D} = \frac{2AB}{N}, \\
	\end{split}
\end{equation}
where $N = (A+B)^2 + (C+D)^2$, and
\begin{equation}
	\begin{split}
		A = \innerp{\Phi^+}{\hat\rho\Phi^+}, & \qquad
		B = \innerp{\Psi^-}{\hat\rho\Psi^-}, \\
		C = \innerp{\Psi^+}{\hat\rho\Psi^+}, & \qquad
		D = \innerp{\Phi^-}{\hat\rho\Phi^-}, \\
	\end{split}
\end{equation}
where $\hat\rho$ is density matrix of output, supposing all input particles are in the same state. If initial state belongs to family
\begin{equation}
	\hat\rho = F\ket{\Phi^+}\bra{\Phi^+} + \frac{1-F}{3}(\ket{\Psi^-}\bra{\Psi^-} + \ket{\Psi^+}\bra{\Psi^+} + \ket{\Phi^-}\bra{\Phi^-})
\end{equation}
(called Werner family) then for $F>1/2$ after sufficient number of iterations we will obtain $\ket{\Phi^+}$ as a result. Figure (\ref{fig:purification}) depicts this process.
\begin{figure}[hbt]
	\centering\includegraphics[width=0.75\linewidth]{images/{purification}.eps}
	\caption{Fidelity of distilled Werner state in 10 steps of distillation process. States with initial fidelity bigger than $\frac12$ become more and more similar to $\ket{\Phi^+}$.\label{fig:purification}}
\end{figure}

\vspace{\baselineskip}

Above examples show it is not unreasonable to talk about amount of entanglement (from now we can skip quotation marks around word ,,amount''). In next chapter we discuss formalism allowing us to quantify entanglement.

