#!/bin/bash

t=../../thesis/images/$1/
mkdir -p $t

case $1 in
	4l_2d_bloch)
		cd ../progs/visualizator
		./visualizator.py -l 4 -s l1,l12  --nolabels -o $t/A.eps --size 3
		./visualizator.py -l 4 -s l1,l14  --nolabels -o $t/B.eps --size 3
		./visualizator.py -l 4 -s l1,l14  --nolabels -o $t/B.eps --size 3
		./visualizator.py -l 4 -s l1,l12  --nolabels -o $t/A.eps --size 3
		./visualizator.py -l 4 -s l1,l15  --nolabels -o $t/C.eps --size 3
		./visualizator.py -l 4 -s l1,l8   --nolabels -o $t/D.eps --size 3
		./visualizator.py -l 4 -s l3,l4   --nolabels -o $t/E.eps --size 3
		./visualizator.py -l 4 -s l3,l12  --nolabels -o $t/F.eps --size 3
		./visualizator.py -l 4 -s l5,l8   --nolabels -o $t/G.eps --size 3
		./visualizator.py -l 4 -s l8,l12  --nolabels -o $t/H.eps --size 3
		./visualizator.py -l 4 -s l8,l13  --nolabels -o $t/I.eps --size 3
		./visualizator.py -l 4 -s l8,l15  --nolabels -o $t/J.eps --size 3
		./visualizator.py -l 4 -s l14,l15 --nolabels -o $t/K.eps --size 3
	;;
	4l_2d_entangled)
		cd ../progs/visualizator
		./visualizator.py -l 4 -s l6,l15 --nolabels --entangled -o $t/CK.eps --size 4
		./visualizator.py -l 4 -s l9,l15 --nolabels --entangled -o $t/KC.eps --size 4
		./visualizator.py -l 4 -s l6,l8  --nolabels --entangled -o $t/GH.eps --size 4
		./visualizator.py -l 4 -s l8,l9  --nolabels --entangled -o $t/HG.eps --size 4
		./visualizator.py -l 4 -s l3,l9  --nolabels --entangled -o $t/EF.eps --size 4
		./visualizator.py -l 4 -s l3,l6  --nolabels --entangled -o $t/FE.eps --size 4
	;;
	results_2d)
		cd ../progs/distance

		./plot.py -n -d $t --ylim 1.0 -i result -s Werner -l 'upper left'
		mv $t/[* $t/Werner.entanglement.eps

		./plot.py -n -d $t --ylim 1.0 -i result -s Horodecki -l 'upper center'
		mv $t/[* $t/Horodecki.entanglement.eps

		./plot.py -n -d $t --ylim 1.0 -i result -s Chen1 -l 'upper left'
		mv $t/[* $t/Chen1.entanglement.eps

		./plot.py -n -d $t --ylim 1.0 -i result -s Chen2 -l 'upper left'
		mv $t/[* $t/Chen2.entanglement.eps
	;;
	evolution)
		cd ../progs/distance

		./plot.py -d $t -i few

		./plot.py -d $t -i evolution
		cp $t/Werner.entropy.evolution.evolution.eps $t/evolution.comparison.eps
		./plot.py -d $t -i evolution --log
		cp $t/Werner.entropy.evolution.evolution.eps $t/evolution.comparison.log.eps
	;;
	*)
		rm -r $t
		echo "Unknown option '$1'"
	;;
esac

