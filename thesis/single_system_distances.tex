\section{Distance measures between states}

Just like in classical theory of probability, one can define functions describing similarity between two quantum probability distributions (density matrices). In this section we will discuss two of this functions --- trace distance and fidelity.

\subsection{Trace distance}

Classical version of trace distance is given by equation:
\begin{equation}
	D(p_i,q_i) = \frac12 \sum\limits_i \left|p_i-q_i\right|.
\end{equation}
Its quantum counterpart is given by:
\begin{equation}
	D(\hat\rho,\hat\sigma) = \frac12 \tr \left|\hat\rho-\hat\sigma\right|.
\end{equation}
where $|\A| = \sqrt{\A^\dagger\A}$.

Both of them satisfy conditions for metric. In case $\rho$ and $\sigma$ commute, i.e. they have the same set of eigenvectors, $\rho=\sum_ir_i\ket{i}\bra{i}$ and $\sigma=\sum_is_i\ket{i}\bra{i}$, $D(\rho,\sigma)$ reproduces $D(p_i,q_i)$:
\begin{equation}
	\begin{aligned}
		D(\rho,\sigma)
		&=
		\frac12 \tr \left| \sum\limits_i (r_i-s_i) \ket{i}\bra{i} \right|
		=
		\frac12 \tr \left( \sum\limits_i |r_i-s_i| \ket{i}\bra{i} \right)
		=\\&=
		\frac12 \sum\limits_i |r_i-s_i|
		=
		D(r_i,s_i).
	\end{aligned}
\end{equation}
%If we express $\rho$ and $\sigma$ with matrices $\hat\lambda$: $\rho=\frac1N \left(\identity + \vec{r}\cdot\vec{\hat\lambda} \right)$ and $\sigma=\frac1N \left(\identity + \vec{s}\cdot\vec{\hat\lambda} \right)$ then $D(\rho,\sigma)$ is proportional to simple geometrical distance between vectors $\vec{r}$ and $\vec{s}$:
%\begin{equation}
%	\begin{aligned}
%		D(\rho,\sigma)
%		&=
%		\frac1{2N} \tr \left|\left(\vec{r}-\vec{s}\right)\cdot\vec{\hat\lambda}\right|	
%	\end{aligned}
%\end{equation}

\subsection{Fidelity}

Another measure of similarity between two probability distributions is the fidelity. Its classical version is given by:
\begin{equation}
	F(p_i,q_i) = \left( \sum\limits_i \sqrt{p_iq_i} \right)^2.
\end{equation}
Its quantum counterpart is given by:
\begin{equation}
	F(\hat\rho,\hat\sigma) = \left( \tr \sqrt{ \sqrt{\hat\rho} \, \hat\sigma \sqrt{\hat\rho} } \right)^2.
\end{equation}
In case $\hat\rho$ and $\hat\sigma$ commute $F(\hat\rho,\hat\sigma)$ reproduces $F(p_i,q_i)$:
\begin{equation}
	\begin{aligned}
		F(\hat\rho,\hat\sigma)
		&=
		\left( \tr \sqrt{\hat\rho\hat\sigma} \right)^2
		=
		\left( \tr \sqrt{ \sum\limits_i r_is_i \ket{i}\bra{i} } \right)^2
		=
		\left( \tr \sum\limits_i \sqrt{r_is_i} \ket{i}\bra{i} \right)^2
		=\\&=
		\left( \sum\limits_i \sqrt{r_is_i} \right)^2
		=
		F(r_i,s_i).
	\end{aligned}
\end{equation}
Unlike in case of trace distance, fidelity is not a metric.

Trace distance and fidelity are related to each other by relation:
\begin{equation}
	1-\sqrt{F(\hat\rho,\hat\sigma)}
	\le
	D(\hat\rho,\hat\sigma)
	\le
	\sqrt{1-F(\hat\rho,\hat\sigma)}.
\end{equation}

In \cite{miszczak_2009} Miszczak et al. introduce upper and lower bound for fidelity which they propose to call superfidelity $G$ and subfidelity $E$ accordingly:
\begin{subequations}
	\begin{align}
		G(\hat\rho, \hat\sigma) &= \tr{\hat\rho\hat\sigma} + \sqrt{(\tr{\hat\rho})^2 -\tr{\hat\rho}^2} \sqrt{(\tr{\hat\sigma})^2 -\tr{\hat\sigma}^2} \\
		E(\hat\rho, \hat\sigma) &= \tr{\hat\rho\hat\sigma} + \sqrt{2((\tr{\hat\rho\hat\sigma})^2 -\tr{(\hat\rho\hat\sigma)^2)}}.
	\end{align}
\end{subequations}

