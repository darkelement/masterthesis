%\subsection{Convex sets}

Let's start with definition:
\begin{definition}[Convex set]
	We say set with structure of linear vector space $C$ is \emph{convex} if for all $x, y \in C$ and for all $t \in [0,1]$, the point $(1-t)x + ty \in C$.
\end{definition}
In other words, set $C$ is convex if every point on line segment between every pair of points from $C$ is also member of $C$. For illustration let's look at figures [\ref{fig:convex:triangle}-\ref{fig:convex:circle}] --- sets depicted there are convex, but set depicted on figure [\ref{fig:convex:nonconvex}] is not convex, because there exist points, that lie on line segment joining two points from set, but don't belong to this set.

\begin{figure}[hbt]
	\centering{
		\subfloat[Non-convex set]{
			\label{fig:convex:nonconvex}
			\begin{tikzpicture}
				\coordinate (x) at (-0.125in, -0.5in);
				\coordinate (y) at ( 0.5in,    0.125in);
				\draw[fill=gray!50, line width=2pt] (0in, 0in) -- (0in, -1in) -- (-1in, 0in) -- (0in, 1in) -- (1in, 0in) -- cycle;
				\draw[line width=2pt, color=blue!50] (x) -- (y);
				\fill[color=blue] (x) circle (3pt) node[color=black, left] {$x$};
				\fill[color=blue] (y) circle (3pt) node[color=black, above] {$y$};
			\end{tikzpicture}
		}
		\hspace{1cm}
		\subfloat[Convex set --- triangle. Three extreme points.]{
			\label{fig:convex:triangle}
			\begin{tikzpicture}
				\draw[fill=gray!50, line width=2pt] (-1in, -1in) -- (0in, 1in) -- (1in, -1in) -- cycle;
				\fill[color=red] (-1in, -1in) circle (3pt);
				\fill[color=red] ( 0in,  1in) circle (3pt);
				\fill[color=red] ( 1in, -1in) circle (3pt);
			\end{tikzpicture}
		}
	}
	\centering{
		\subfloat[Convex set --- square. Four extreme points --- some points may be represented ambiguously.]{
			\label{fig:convex:square}
			\begin{tikzpicture}
				\draw[fill=gray!50, line width=2pt] (0.25in, 0.25in) rectangle (1.75in, 1.75in);
				\fill[color=red] (0.25in, 0.25in) circle (3pt);
				\fill[color=red] (0.25in, 1.75in) circle (3pt);
				\fill[color=red] (1.75in, 1.75in) circle (3pt);
				\fill[color=red] (1.75in, 0.25in) circle (3pt);
				\path (0in, 0in) -- (2in, 2in);
			\end{tikzpicture}
		}
		\hspace{1cm}
		\subfloat[Convex set --- circle. Infinitely many extreme point.]{
			\label{fig:convex:circle}
			\begin{tikzpicture}
				\draw[fill=gray!50, draw=red, line width=2pt] (0, 0) circle (1in);
			\end{tikzpicture}
		}
	}
	\caption{Illustration of convexity of sets. Border points are marked by black color, extreme points by red color. \label{fig:convex}}
\end{figure}

Every linear combination in form
\begin{equation}
	\sum_i p_ix_i, \qquad \mathrm{where} \quad \sum_i p_i = 1 \quad \mathrm{and} \quad p_i \geq 0\ \forall i
\end{equation}
we will call a convex combination.

\begin{definition}[Extreme point]
	Point in $C$ which does not lie in any open line segment joining two points of $C$ is called \emph{extreme point}.
\end{definition}
In other words, we say a point $x \in C$ is extreme if it can not be expressed as convex combination of \emph{other} points from $C$. Set of extreme points is always subset of all border points. We denote set of extreme points of $C$ as $\partial_eC$ using subindex $e$ to distinguish them from set of border points $\partial C$.

\begin{definition}[Convex hull]
	\emph{Convex hull} or \emph{convex envelope} of a set $X$ of points from linear space ($\conv X$) is the smallest convex set that contains X.
\end{definition}

From definition of convex set it is obvious that any element of convex set can be represented as convex combination of some number of extreme points. Caratheodory's theorem gives us limit for this number:
\begin{theorem}[Caratheodory's theorem]
	Let $A \subset \mathbbm{R}^N$ be subset of $N$-dimensional linear space, then any $x \in \conv A$ can be expressed in the form
	\[x = \sum\limits_{n=1}^{N+1}p_na_n\]
	where $\sum\limits_{n=1}^{N+1}p_n = 1$, and, for $n = 1,\ldots,N+1$, $p_n \geq 0$ and $a_n\in A$.
\end{theorem}
For each element of convex set we may have to use different extreme points, but $N+1$ of them is always enough. For example triangle (figure [\ref{fig:convex:triangle}]) has three extreme points and any point inside it can be written inambiguously as convex combination of them, it is a so called \emph{simplex}. In square (figure [\ref{fig:convex:square}]) there are four extreme points. According to Caratheodory's theorem it is enough to use three of them, so some combinations may be ambiguous. In circle (figure [\ref{fig:convex:circle}]) there is infinite number of possible decompositions. Therefore neither square nor circle is simplex.

