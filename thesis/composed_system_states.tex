\section{Composed systems}

This section shows how to construct Hilbert case of composed quantum systems and introduces notion of entanglement. It is based mainly on work done in \cite{jakobczyk_structure}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Hilbert space of composed systems}

As entanglement is phenomenon manifesting itself only in composed systems we need to introduce Hilbert space for such. In classical physical we use cartesian product ,,$\times$'' to build state space of composed system from state spaces of its subsystems. Since pure state in quantum physics is \memph{projective} Hilbert space we need more subtle construction. It is postulated that space of pure states of composed system is \memph{tensor product} of component Hilbert spaces: $\Hilb_{AB} = \Hilb_A\otimes\Hilb_B$. For simplicity we will consider only systems with finite-dimensional Hilbert spaces.

Inner product of states from composed Hilbert space is given by:
\begin{equation}
	\innerp{\psi_A\otimes\phi_B}{\chi_A\otimes\eta_B}_{AB} \eqdef \innerp{\psi_A}{\chi_A}_A\innerp{\phi_B}{\eta_B}_B
\end{equation}

Let $\{\ket{e_n}_A\}_{n=1}^{N}$ be orthonormal basis in $\Hilb_A^N$ and $\{\ket{e_m}_B\}_{m=1}^M$ be orthonormal basis in $\Hilb_B^M$. Then
\begin{equation}
	\{\ket{e_n\otimes e_m}_{AB}\}_{n,m=1}^{n=N,m=M}
\end{equation}
is orthonormal basis in $N\cdot M$-dimensional composed Hilbert space $\Hilb_A^N \otimes \Hilb_B^M$. If
\begin{equation}
	\ket\phi_A = \sum\limits_{n=1}^N c_n\ket{e_n}_A,
	\qquad
	\ket\psi_B = \sum\limits_{m=1}^M d_m\ket{e_m}_B
\end{equation}
then any tensor product of vectors from component spaces $\ket{\phi\otimes\psi}_{AB}$ can be written using basis vectors:
\begin{equation}
	\label{eq:tensor_pure}
	\ket{\phi\otimes\psi}_{AB}
		= \left(\sum\limits_{n}c_n\ket{e_n}_A\right) \otimes \left(\sum\limits_md_m\ket{e_m}_{B}\right)
		= \sum\limits_{m,n}c_nd_m\ket{e_n\otimes e_m}_{AB}.
\end{equation}

Note that in general vector $\ket\Phi$ from Hilbert space $\Hilb_{AB}$ can be written as:
\begin{equation}
	\ket\Phi = \sum\limits_{n,m}b_{nm}\ket{e_n\otimes e_m}_{AB}.
\end{equation}
$\ket\Phi$ \memph{not} always can be written in tensor form $\ket{\phi\otimes\psi}$ because $b_{nm}$ not necessarily factorizes to $c_nd_m$ as in equation (\ref{eq:tensor_pure}). Example of such states are \memph{Bell states}:
\begin{subequations}
	\begin{align}
		\ket{\Psi^\pm} = \frac{1}{\sqrt{2}}(\ket0 \otimes \ket1 \pm \ket1 \otimes \ket0) \\
		\ket{\Phi^\pm} = \frac{1}{\sqrt{2}}(\ket0 \otimes \ket0 \pm \ket1 \otimes \ket1)
	\end{align}
\end{subequations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Operators on composed Hilbert space}

Tensor product $\C = \A\otimes\B$ of linear operators $\A$ and $\B$ acting on spaces $\Hilb_A$ and $\Hilb_B$ respectively is defined as follows:
\begin{equation}
	(\A\otimes\B) \ket{\phi\otimes\psi} \eqdef \ket{\A\phi\otimes\B\psi}.
\end{equation}
Some properties:
\begin{itemize}
	\item product of tensor products is tensor product of products: $(\A\otimes\B)(\C\otimes\D) = \A\C\otimes\B\D$;
	\item in case on finite-dimentional Hilbert spaces tensor product is Kronecker product of matrices representing operators: $\innerp{e_{k}\otimes e_{l}}{\A\otimes\B\ e_m\otimes e_n} = A_{km}B_{ln}$;
	\item if $a$ is eigenvalue of operator $\A$: $\A\ket\phi = a\ket\phi$ then eigenvalue of simple extension $\A\otimes\identity_B$ is also $a$: $(\A\otimes\identity_B)\ket{\phi\otimes\psi} = a\ket{\phi\otimes\psi}$;
	\item simple extensions of operators from different spaces commute: $\commutator{\A\otimes\mathbbm{1}_B}{\mathbbm{1}_A\otimes\B} = \hat{0}$;
	\item trace of tensor product is product of traces: $\tr{\A\otimes\B} = \tr\A \tr\B$.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Entanglement}

After this introduction we are ready to define \memph{separability} and \memph{entanglement}.

\begin{definition}[Separability]
	We call quantum state \memph{separable (disentangled)} if its density matrix can be written as following convex combination:
	\begin{equation}
		\hat\rho = \sum_i p_i \hat\rho_i^A \otimes \hat\rho_i^B
	\end{equation}
	where $\hat\rho_i$'s are density matrices corresponding to states of component spaces.
\end{definition}

This decomposition is ambiguous and may be satisfied by (infinitely) many combinations of pure stats. Caratheodory's theorem tells us that in practise we can restrict ourselves to sums for $i=1,\ldots,\dim{\Hilb_{AB}}$.

In case of pure states definition of separability simplifies and we can say that state $\ket\Phi$ is separable if it can be written as tensor product:
\begin{equation}
	\ket\Phi = \ket\phi_A \otimes \ket\psi_B
\end{equation}

\begin{definition}[Entanglement]
	We call quantum state \memph{entangled (inseparable)} if it is \memph{not} separable.
\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Peres-Horodecki criterion}

Having defined separability we need criterion that will allow us to easily distinguish separable from inseparable states. One of them is positive partial transpose (PPT) criterion:
\begin{theorem}[Partial positive transpose criterion]
	If $\hat\rho_{AB}$ is separable then the new matrix $\hat\rho_{AB}^{T_B}$ with matrix elements defined in some fixed product basis as:
	\begin{equation}
		\bra{m\otimes\mu}\hat\rho_{AB}^{T_B}\ket{n\otimes\nu} = \bra{m\otimes\nu}\hat\rho_{AB}\ket{n\otimes\mu}
	\end{equation}
	is a density operator (i.e. is positive).
\end{theorem}

This criterion is consequence of more general approach exploiting positive, but not completely positive\footnote{We say map $\Theta$ is positive when it maps any positive operator on $\Hilb_B$ into a positive one and is completely positive if $\identity\otimes\Theta$ is positive for identity map $\identity$ of any finite-dimensional system.} (PnCP) maps. Any separable quantum state transformed by map in form $\identity\otimes\Theta$ where $\Theta$ is PnCP map must stay positive. If $(\identity\otimes\Theta)\hat\rho_{AB}$ is negative we have sure that state described by $\hat\rho_{AB}$ is inseparable (still there may be inseparable states for which positivity is preserved). In above example map $\Theta$ was transposition acting on second system.

Using the above technique one can provide a necessary and sufficient condition for separability:
\begin{theorem}[Peres-Horodecki criterion]
	The states form $\Hilb_A\otimes\Hilb_B$ satisfying $\dim{\Hilb_A}\dim{\Hilb_B} \leq 6$ (two-qubits or qubit-qutrit systems) are separable if and only if they are PPT.
\end{theorem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Partial trace}

We have learned that we can construct state of composed system knowing states of its subsystems using tensor product: $\hat\rho_{AB} = \hat\rho_A\otimes\hat\rho_B$. We also need to know how to determine state of subsystem knowing state of whole composed system that the first is part of. To do so we introduce partial trace:
\begin{definition}[Partial trace]
	Let $\{e_k^B\}$ be orthonormal basis in $\Hilb_B$. Partial trace $\tr_B$ over subsystem $B$ is defined by:
	\begin{equation}
		\innerp{\psi_A}{\tr_B\hat\rho\,\phi_A}_A \eqdef \sum\limits_k\innerp{\psi_A\otimes e_k^B}{\hat\rho(\phi_A\otimes e_k^B)}_{AB}
	\end{equation}
	Analogically one can define partial trace over subsystem $A$.
\end{definition}

Partial trace of density matrix of composed  system $AB$ over system $B$ is density matrix corresponding to system $A$:
\begin{equation}
	\tr_B(\States_A\otimes\States_B) = \States_A
\end{equation}
and \textit{vice versa}. One can also show that:
\begin{itemize}
	\item[\textbullet] $\tr_A\hat\rho\geq 0$, $\tr_B\hat\rho\geq 0$, $\tr(\tr_A\hat\rho)=1$, $\tr(\tr_B\hat\rho)=1$;
	\item[\textbullet] $\tr_A(\hat\rho_A\otimes\hat\rho_B)=\hat\rho_B$, $\tr_B(\hat\rho_A\otimes\hat\rho_B)=\hat\rho_A$.
\end{itemize}

\begin{theorem}
	\begin{equation}
		\tr(\hat\rho(\X_A\otimes\identity_B)) = \tr((\tr_B\hat\rho)\X_A)
	\end{equation}
\end{theorem}
\begin{proof}
	Let $\{e_j^A\}$ and $\{e_k^B\}$ be orthonormal bases in $\Hilb_A$ and $\Hilb_B$. Then
	\begin{equation}
		\begin{split}
		\tr\left(\hat\rho(\X_A\otimes\identity_B)\right)
			&= \sum\limits_{j,k}\innerp{e_j^A\otimes e_k^B}{\hat\rho(\X_A\otimes\identity_B)(e_j^A\otimes e_k^B)}_{AB} \\
			&= \sum\limits_{j,k}\innerp{e_j^A\otimes e_k^B}{\hat\rho((\X_Ae_j^A)\otimes e_k^B)}_{AB} \\
			&= \sum\limits_{j}\innerp{e_j^A}{(\tr_B\hat\rho)\X_A e_j^A}_A \\
			&= \tr\left((\tr_B\hat\rho)\X_A\right)
		\end{split}
	\end{equation}
\end{proof}
By virtue of above theorem and properties of partial trace we can say that partial trace over system $B$ is state of system $A$.

\begin{example}
	Let's consider Hilbert space $\Hilb_4 = \mathbbm{C}^2\otimes\mathbbm{C}^2$ with basis $\ket{e_1\otimes e_1}$, $\ket{e_1\otimes e_2}$, $\ket{e_2\otimes e_1}$, $\ket{e_2\otimes e_2}$. Because
	\begin{equation}
		\begin{split}
			\innerp{e_1}{\tr_B\hat\rho\,e_1} = \innerp{e_1\otimes e_1}{\hat\rho\,e_1\otimes e_1} + \innerp{e_1\otimes e_2}{\hat\rho\,e_1\otimes e_2} = \rho_{11}+\rho_{22} \\
			\innerp{e_1}{\tr_B\hat\rho\,e_2} = \innerp{e_1\otimes e_1}{\hat\rho\,e_2\otimes e_1} + \innerp{e_1\otimes e_2}{\hat\rho\,e_2\otimes e_2} = \rho_{13}+\rho_{24} \\
			\innerp{e_2}{\tr_B\hat\rho\,e_1} = \innerp{e_2\otimes e_1}{\hat\rho\,e_1\otimes e_1} + \innerp{e_2\otimes e_2}{\hat\rho\,e_1\otimes e_2} = \rho_{31}+\rho_{42} \\
			\innerp{e_2}{\tr_B\hat\rho\,e_2} = \innerp{e_2\otimes e_1}{\hat\rho\,e_2\otimes e_1} + \innerp{e_2\otimes e_2}{\hat\rho\,e_2\otimes e_2} = \rho_{33}+\rho_{44} \\
		\end{split}
	\end{equation}
  we can write
	\begin{equation}
		\tr_B\hat\rho = \begin{pmatrix}\rho_{11}+\rho_{22} & \rho_{13}+\rho_{24} \\ \rho_{31}+\rho_{42} & \rho_{33}+\rho_{44}\end{pmatrix}.
	\end{equation}
	In the same way we can show that
	\begin{equation}
		\tr_A\hat\rho = \begin{pmatrix}\rho_{11}+\rho_{33} & \rho_{12}+\rho_{34} \\ \rho_{21}+\rho_{43} & \rho_{22}+\rho_{44}\end{pmatrix}.
	\end{equation}
\end{example}

\begin{example}
	\label{ex:mix}
	Let's consider \memph{pure}, entangled state $\ket{\Phi^+} = \frac{1}{\sqrt{2}}\left(\ket{e_1\otimes e_1} + \ket{e_2\otimes e_2}\right)$. Density matrix of this state has form
	\begin{equation}
		\hat\rho_{\Phi^+} = \frac12\begin{pmatrix}1&0&0&1\\0&0&0&0\\0&0&0&0\\1&0&0&1\end{pmatrix}.
	\end{equation}
	Despite that it is pure state, its subsystem is not in pure state because
	\begin{equation}
		\tr_B\hat\rho_{\Phi^+} = \frac12\begin{pmatrix}1&0\\0&1\end{pmatrix}
	\end{equation}
	and $(\tr_B\hat\rho_{\Phi^+})^2 = \frac14\identity \neq \tr_B\hat\rho_{\Phi^+}$ which is an astonishing fact and leads to such unintuitive phenomenons as e.g. negative information. This also means entangled systems must always be considered ,,as a whole'', never as separated parts, even if this parts are very far from each other.
\end{example}

