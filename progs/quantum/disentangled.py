# -*- coding: utf-8 -*-

import numpy

def ctos(c):
	result =  '%.4f ' % c.real
	result += '-' if c.imag < 0 else '+'
	result += ' %.4f i' % abs(c.imag)
	return result


class State:
	"""Disentangled d-partite state class

	Provides methods for generating states matrix, crossover and mutations"""

	def __init__(self, d, psi, phi, ksi, diff=0.2):
		self.d = d
		self.N = numpy.power(4, d) # dimensionality of space + 1
		self.n = numpy.power(2, d) # size of state matrix (n by n)
		self.matrix = None # matrix representation of state
		self.diff = diff # evolution parameter

		self.psi = numpy.array(psi)
		self.phi = numpy.array([list(p) for p in phi])
		self.ksi = numpy.array([list(k) for k in ksi])

	@classmethod
	def init_random(cls, d, diff=0.2):
		N = numpy.power(4, d)
		#roots = [0.0, 0.5*numpy.pi, 1.5*numpy.pi, 2.0*numpy.pi]

		psi =  2.0 * numpy.pi * numpy.random.rand(N)
		phi = [2.0 * numpy.pi * numpy.random.rand(N) for i in range(0, d)]
		ksi = [2.0 * numpy.pi * numpy.random.rand(N) for i in range(0, d)]

		#r = numpy.random.choice(N)
		#psi[r] = 0
		#for i in range(0, d):
		#	phi[i][r] = numpy.random.choice(roots)
		#	ksi[i][r] = numpy.random.choice(roots)
			
		return cls(d, psi, phi, ksi, diff)

	@classmethod
	def init_crossover(cls, first, second):
		"""Perform crossover on two states and return offspring"""

		if first.d != second.d:
			return None
		d = first.d

		return cls(d, cls._cross([first.psi,    second.psi   ]),
				     [cls._cross([first.phi[i], second.phi[i]]) for i in range(0, d)],
				     [cls._cross([first.ksi[i], second.ksi[i]]) for i in range(0, d)],
				      diff=(first.diff + second.diff) / 2.0
				)

	@classmethod
	def init_from_dict(cls, d, diff=0.2):
		return cls(len(d['phi']), d['psi'], d['phi'], d['ksi'], 0.2)

	@classmethod
	def init_from_vector(cls, d, v, diff=0.2):
		N = numpy.power(4, d)
		psi = v[0:N]
		phi = [v[i*N:(i+1)*N] for i in range(1, d+1)]
		ksi = [v[i*N:(i+1)*N] for i in range(d+1, d+d+1)]
		return cls(d, psi, phi, ksi, diff)

	# -----------------------------------------------------------------------------------

	def latex(self):
		if self.matrix == None:
			self.get_matrix()

		result = '\left[\\begin{array}{%s}\n' % ('r'*len(self.matrix))
		for r in self.matrix:
			for j, c in enumerate(r):
				result += '\t%s' % ctos(c)
				if j != len(r)-1:
					result += ',\t&'
			result += '\\\\\n'
		result += '\end{array}\\right]\n'
		return result

	def dict(self):
		return {
		        'psi': list(self.psi),
		        'phi': [list(l) for l in self.phi],
		        'ksi': [list(l) for l in self.ksi]
		    }

	# -----------------------------------------------------------------------------------
	# Comparison of two states

	def __eq__(self, other):
		if not self.d == other.d:
			return False

		if not (self.psi == other.psi).all():
			return False

		if not (self.phi == other.phi).all():
			return False

		if not (self.ksi == other.ksi).all():
			return False

		return True

	def __ne__(self, other):
		return not self.__eq__(other)

	# -----------------------------------------------------------------------------------

	def copy(self):
		"""Copies state"""
		copy = State(self.d, self.psi, self.phi, self.ksi)
		copy.matrix = self.matrix
		copy.diff = self.diff
		return copy

	def change_evolution_parameter(self, multiplier):
		self.diff *= multiplier

	# -----------------------------------------------------------------------------------

	def mutation(self):
		"""Perform mutation on state"""

		# mutation of state
		self._mutate(self.psi)
		for tab in self.phi: self._mutate(tab)
		for tab in self.ksi: self._mutate(tab)

		self.matrix = None

	# -----------------------------------------------------------------------------------

	def get_matrix(self):
		"""Returns matrix representation of state"""

		# If we calculated matrix before just return it
		if self.matrix != None:
			return self.matrix

		# Ensure right value of psi_0 (could change during mutation)
		self.psi[0] = 0.5 * numpy.pi

		# Values of sines and cosines we use later
		cos_psi = numpy.cos(self.psi)
		sin_psi = numpy.sin(self.psi)
		cos_phi = [numpy.cos(tab) for tab in self.phi]
		sin_phi = [numpy.sin(tab) for tab in self.phi]

		# Probabilities
		c = 1.0
		p = numpy.array([0.0 for i in range(0, self.N)])
		for i in range(self.N-1, -1, -1):
			p[i] = sin_psi[i] * sin_psi[i] * c * c
			c *= cos_psi[i]

		# Calculating matrix
		result = numpy.zeros((self.n,self.n), dtype=numpy.dtype('complex'))
		for i in range(0, self.N):
			subs = [self._get_sub(cos_phi[j][i], sin_phi[j][i], self.ksi[j][i])
			        for j in range(0, self.d)]
			result += p[i] * self._kronecker(subs)

		# Save and return result
		self.matrix = result
		return result

	# -----------------------------------------------------------------------------------
	# Tools

	@staticmethod
	def _cross(tab):
		"""Perform cross-over on one 'chromatin fiber'"""
		n = numpy.random.randint(0, 2)
		r = numpy.random.randint(0, len(tab[0]))
		return numpy.append(tab[n][:r], tab[abs(n-1)][r:])

	def _mutate(self, tab):
		"""Perform mutation on one 'chromatin fiber'"""
		for i in range(0, len(tab)):
			tab[i] += self.diff * (2.0*numpy.random.random() - 1.0)

	def _get_sub(self, c, s, d):
		"""Return matrix of sub-state"""
		return numpy.matrix([
			    [         c*c        , c*s*numpy.exp(1j*d)],
			    [c*s*numpy.exp(-1j*d),         s*s        ]
			])

	def _kronecker(self, tab):
		"""Calculate kronecker product of all matrices in 'tab'"""
		result = tab[0]
		for i in range(1, self.d):
			result = numpy.kron(result, tab[i])
		return result

