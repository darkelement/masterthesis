# -*- coding: utf-8 -*-

import numpy
sqrt = numpy.sqrt
s3 = sqrt(3.0)
s6 = sqrt(6.0)
rs3 = 1.0 / s3
rs6 = 1.0 / s6


# Convention:
#
# 〈0| ~ (0,1)
# 〈1| ~ (1,0)
#
# 〈00| ~ (0,0,0,1)
# 〈01| ~ (0,0,1,0)
# 〈10| ~ (0,1,0,0)
# 〈11| ~ (1,0,0,0)
#
# etc...

##########################################################################################
# Werner

class Werner:
	name = 'Werner'
	a, b = 0.0, 1.0
	param = 'F'
	dims = 2

	def __init__(self, F=0.5):
		self.x = [F]
		self.matrix = F * numpy.matrix([
			    [0.0,  0.0,  0.0, 0.0],
			    [0.0,  0.5, -0.5, 0.0],
			    [0.0, -0.5,  0.5, 0.0],
			    [0.0,  0.0,  0.0, 0.0]
			]) + (1.0 - F) / 3.0 * numpy.matrix([
			    [1.0, 0.0, 0.0, 0.0],
			    [0.0, 0.5, 0.5, 0.0],
			    [0.0, 0.5, 0.5, 0.0],
			    [0.0, 0.0, 0.0, 1.0]
			])

##########################################################################################
# Horodecki

class Horodecki:
	name = 'Horodecki'
	a, b = 0.0, 1.0
	param = 'q'
	dims = 2

	def __init__(self, q=0.5, a=0.75):
		self.x = [q, a]
		a2 = a*a
		self.matrix = numpy.matrix([
			    [     q*a2     ,        0.0        ,         0.0       , q*a*sqrt(1-a2)],
			    [     0.0      ,    (1-q)*(1-a2)   , (1-q)*a*sqrt(1-a2),       0.0     ],
			    [     0.0      , (1-q)*a*sqrt(1-a2),      (1-q)*a2     ,       0.0     ],
			    [q*a*sqrt(1-a2),        0.0        ,         0.0       ,     q*(1-a2)  ]
			])

##########################################################################################
# Chen

class Chen1:
	name = 'Chen1'
	a, b = 0.0, 1.0
	param = '$\lambda_3$'
	dims = 2

	def __init__(self, l=0.0):
		self.x = [l]
		l2 = l / 2.0
		self.matrix = numpy.matrix([
			    [ l2, 0.0, 0.0,   -l2],
			    [0.0, 0.0, 0.0,   0.0],
			    [0.0, 0.0, 1.0-l, 0.0],
			    [-l2, 0.0, 0.0,    l2]
			])

class Chen2:
	name = 'Chen2'
	a, b = 0.0, 1.0
	param = '$\lambda_3$'
	dims = 2

	def __init__(self, l=0.0):
		self.x = [l]
		l2 = l / 2.0
		self.matrix = numpy.matrix([
			    [ l2,      0.0,         0.0,    -l2],
			    [0.0, 0.2*(1.0-l),      0.0,    0.0],
			    [0.0,      0.0,    0.8*(1.0-l), 0.0],
			    [-l2,      0.0,         0.0,     l2]
			])

##########################################################################################
# 3-qubit states

class D3:
	a, b = 0.0, 1.0
	param = 'p'
	dims = 3

	def __init__(self, p, V1, V2):
		self.c = [p]
		v1 = numpy.matrix([V1]) / sqrt(sum([n*n for n in V1]))
		v2 = numpy.matrix([V2]) / sqrt(sum([n*n for n in V2]))
		v = sqrt(p) * v1 + sqrt(1.0-p) * v2
		M = v.transpose() * v
		self.matrix = M / numpy.trace(M)

# ----------------------------------------------------------------------------------------
#                                                    〈000|+〈111|,  〈100|+〈010|+〈001|
class D3_0(D3):
	name = '3d0'
	def __init__(self, p=0.5): D3.__init__(self, p, [1,0,0,0,0,0,0,1], [0,0,0,1,0,1,1,0])

# ----------------------------------------------------------------------------------------
#                                                           〈000|+〈111|,  〈010|+〈100|
class D3_1(D3):
	name = '3d1'
	def __init__(self, p=0.5): D3.__init__(self, p, [1,0,0,0,0,0,0,1], [0,0,0,-1,0,0,1,0])

# ----------------------------------------------------------------------------------------
#                                                                  〈000|+〈111|,  〈010|
class D3_2(D3):
	name = '3d2'
	def __init__(self, p=0.5): D3.__init__(self, p, [1,0,0,0,0,0,0,1], [0,0,0,0,0,0,1,0])

# ----------------------------------------------------------------------------------------
#                                                                         〈000|,  〈111|
class D3_3(D3):
	name = '3d3'
	def __init__(self, p=0.5): D3.__init__(self, p, [0,0,0,0,0,0,0,1], [1,0,0,0,0,0,0,0])

# ----------------------------------------------------------------------------------------
#                                                           〈000|+〈111|,  〈000|-〈111|
class D3_4(D3):
	name = '3d4'
	def __init__(self, p=0.5): D3.__init__(self, p, [1,0,0,0,0,0,0,1], [-1,0,0,0,0,0,0,1])

# ----------------------------------------------------------------------------------------
#                                                           〈000|+〈111|,  〈001|+〈110|
class D3_5(D3):
	name = '3d5'
	def __init__(self, p=0.5): D3.__init__(self, p, [1,0,0,0,0,0,0,1], [0,1,0,0,0,0,1,0])

# ----------------------------------------------------------------------------------------
#                                                                  〈001|,  〈100|+〈010|
class D3_6(D3):
	name = '3d6'
	def __init__(self, p=0.5): D3.__init__(self, p, [0,0,0,0,0,0,1,0], [0,0,0,1,0,1,0,0])

class D3_7(D3):
	name = '3d7'
	def __init__(self, p=0.5): D3.__init__(self, p, [0,0,0,0,0,0,1,0],[0,0,0,1,0,0.5,0,0])

class D3_8(D3):
	name = '3d8'
	def __init__(self, p=0.5): D3.__init__(self, p, [0,0,0,0,0,0,1,0],[0,0,0,0.5,0,1,0,0])

##########################################################################################

def Noised(generator, level):
	class CNoised(generator):
		def __init__(self, x):
			generator.__init__(self, x)
			self.matrix = (1.0-level) * self.matrix + \
			              level * numpy.identity(pow(2, generator.dims))

	return CNoised

##########################################################################################

I2 = numpy.identity(2, dtype=numpy.dtype('complex'))
I3 = numpy.identity(3, dtype=numpy.dtype('complex'))
I4 = numpy.identity(4, dtype=numpy.dtype('complex'))

Pauli = {
		'x': numpy.matrix([[0,  1 ], [1,  0]]),
		'y': numpy.matrix([[0, -1j], [1j, 0]]),
		'z': numpy.matrix([[1,  0 ], [0, -1]])
	}


get = {
		'Werner':    Werner,
		'Horodecki': Horodecki,
		'Chen1':     Chen1,
		'Chen2':     Chen2,
		'3d0':       D3_0,
		'3d1':       D3_1,
		'3d2':       D3_2,
		'3d3':       D3_3,
		'3d4':       D3_4,
		'3d5':       D3_5,
		'3d6':       D3_6,
		'3d7':       D3_7,
		'3d8':       D3_8
	}

traceless = {
	'3': {
			'l1': numpy.matrix([[0,  1,  0 ], [1,  0,  0 ], [0,  0,  0]]) * s3, # 1
			'l2': numpy.matrix([[0, -1j, 0 ], [1j, 0,  0 ], [0,  0,  0]]) * s3, # 2
			'l3': numpy.matrix([[1,  0,  0 ], [0, -1,  0 ], [0,  0,  0]]) * s3, # 3
			'l4': numpy.matrix([[0,  0,  1 ], [0,  0,  0 ], [1,  0,  0]]) * s3, # 4
			'l5': numpy.matrix([[0,  0, -1j], [0,  0,  0 ], [1j, 0,  0]]) * s3, # 5
			'l6': numpy.matrix([[0,  0,  0 ], [0,  0,  1 ], [0,  1,  0]]) * s3, # 6
			'l7': numpy.matrix([[0,  0,  0 ], [0,  0, -1j], [0,  1j, 0]]) * s3, # 7
			'l8': numpy.matrix([[1,  0,  0 ], [0,  1,  0 ], [0,  0, -2]])
		},

	'4': {
			'Chen1(0)':     4*Chen1(0.0).matrix - I4,
			'Chen1(1)':     4*Chen1(1.0).matrix - I4,
			'Chen2(0)':     4*Chen2(0.0).matrix - I4,
			'Chen2(1)':     4*Chen2(1.0).matrix - I4,
			'Horodecki(0)': 4*Horodecki(0.0).matrix - I4,
			'Horodecki(1)': 4*Horodecki(1.0).matrix - I4,
			'Werner(0)':    4*Werner(0.0).matrix - I4,
			'Werner(1)':    4*Werner(1.0).matrix - I4,

			'l1':  numpy.matrix([[ 0, 1, 0, 0 ], [ 1, 0, 0, 0 ],
			                     [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ]]) * s6,
			'l2':  numpy.matrix([[ 0,-1j,0, 0 ], [1j, 0, 0, 0 ],
			                     [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ]]) * s6,
			'l3':  numpy.matrix([[ 1, 0, 0, 0 ], [ 0,-1, 0, 0 ],
			                     [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ]]) * s6,
			'l4':  numpy.matrix([[ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ],
			                     [ 1, 0, 0, 0 ], [ 0, 0, 0, 0 ]]) * s6,
			'l5':  numpy.matrix([[ 0, 0,-1j,0 ], [ 0, 0, 0, 0 ],
			                     [1j, 0, 0, 0 ], [ 0, 0, 0, 0 ]]) * s6,
			'l6':  numpy.matrix([[ 0, 0, 0, 0 ], [ 0, 0, 1, 0 ],
			                     [ 0, 1, 0, 0 ], [ 0, 0, 0, 0 ]]) * s6,
			'l7':  numpy.matrix([[ 0, 0, 0, 0 ], [ 0, 0,-1j,0 ],
			                     [ 0, 1j,0, 0 ], [ 0, 0, 0, 0 ]]) * s6,
			'l8':  numpy.matrix([[ 1, 0, 0, 0 ], [ 0, 1, 0, 0 ],
			                     [ 0, 0,-2, 0 ], [ 0, 0, 0, 0 ]]) * s6 * rs3,
			'l9':  numpy.matrix([[ 0, 0, 0, 1 ], [ 0, 0, 0, 0 ],
			                     [ 0, 0, 0, 0 ], [ 1, 0, 0, 0 ]]) * s6,
			'l10': numpy.matrix([[ 0, 0, 0,-1j], [ 0, 0, 0, 0 ],
			                     [ 0, 0, 0, 0 ], [1j, 0, 0, 0 ]]) * s6,
			'l11': numpy.matrix([[ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ],
			                     [ 0, 0, 0, 0 ], [ 0, 1, 0, 0 ]]) * s6,
			'l12': numpy.matrix([[ 0, 0, 0, 0 ], [ 0, 0, 0,-1j],
			                     [ 0, 0, 0, 0 ], [ 0,1j, 0, 0 ]]) * s6,
			'l13': numpy.matrix([[ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ],
			                     [ 0, 0, 0, 1 ], [ 0, 0, 1, 0 ]]) * s6,
			'l14': numpy.matrix([[ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ],
			                     [ 0, 0, 0,-1j], [ 0, 0, 1j,0 ]]) * s6,
			'l15': numpy.matrix([[ 1, 0, 0, 0 ], [ 0, 1, 0, 0 ],
			                     [ 0, 0, 1, 0 ], [ 0, 0, 0,-3 ]]),

			'kronecker_ix': numpy.kron(I2,         Pauli['x']),
			'kronecker_iy': numpy.kron(I2,         Pauli['y']),
			'kronecker_iz': numpy.kron(I2,         Pauli['z']),
			'kronecker_xi': numpy.kron(Pauli['x'], I2),
			'kronecker_xx': numpy.kron(Pauli['x'], Pauli['x']),
			'kronecker_xy': numpy.kron(Pauli['x'], Pauli['y']),
			'kronecker_xz': numpy.kron(Pauli['x'], Pauli['z']),
			'kronecker_yi': numpy.kron(Pauli['y'], I2),
			'kronecker_yx': numpy.kron(Pauli['y'], Pauli['x']),
			'kronecker_yy': numpy.kron(Pauli['y'], Pauli['y']),
			'kronecker_yz': numpy.kron(Pauli['y'], Pauli['z']),
			'kronecker_zi': numpy.kron(Pauli['z'], I2),
			'kronecker_zx': numpy.kron(Pauli['z'], Pauli['x']),
			'kronecker_zy': numpy.kron(Pauli['z'], Pauli['y']),
			'kronecker_zz': numpy.kron(Pauli['z'], Pauli['z']),
		}
	}

