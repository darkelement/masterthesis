# -*- coding: utf-8 -*-

import numpy
import scipy
import scipy.linalg

##########################################################################################
# Some private constants

__1_over_ln2 = 1.0 / numpy.log(2.0)
__sy_otimes_sy = numpy.kron([[0, -1j], [1j, 0]], [[0, -1j], [1j, 0]])

##########################################################################################
# Distance functions

# ---------------------------------------------------------------------------------------
# Relative entropy

def entropy(state, disentangled):
	t = numpy.trace(state * scipy.linalg.logm(state)
	              - state * scipy.linalg.logm(disentangled))
	return abs(t) * __1_over_ln2

# ---------------------------------------------------------------------------------------
# Hilbert-Schmidt metric

def hilbert_schmidt(state, disentangled):
	D = state - disentangled
	s = numpy.sqrt(numpy.trace(D*D))
	return abs(s)

# reverse of H-S entanglement calculated in maximaly entangled states (Werner(1), 3d5(1))
__1_over_hs_max = {
		4.0: 1.0 / 0.577350,
		8.0: 1.0 / 0.687483
	}

def hilbert_schmidt_normalized(state, disentangled):
	return hilbert_schmidt(state, disentangled) * __1_over_hs_max[len(state)]

# ---------------------------------------------------------------------------------------
# Fidelity

def F(A, B):
	S = scipy.linalg.sqrtm(B)
	s = scipy.linalg.sqrtm(S * A * S)
	return numpy.trace(s)

def bures(state, disentangled):
	return numpy.sqrt(2.0 - 2.0 * abs(F(state, disentangled)))

# reverse of Bures entropy calculated in noised
# maximaly entangled states (Werner(1), 3d5(1))
__1_over_bures_max = {
		4.0: 1.0 / 0.765367,
		8.0: 1.0 / 0.765367
	}

def bures_normalized(state, disentangled):
	return bures(state, disentangled) * __1_over_bures_max[len(state)]

# ---------------------------------------------------------------------------------------
# Superfidelity

def G(A, B):
	eA = [e.real for e in numpy.linalg.eig(A)[0]]
	eB = [e.real for e in numpy.linalg.eig(B)[0]]
	trAB = numpy.trace(A*B).real
	trAA = sum([e*e for e in eA])
	trBB = sum([e*e for e in eB])
	return trAB + numpy.sqrt((1.0 - trAA) * (1.0 - trBB))

def superfidelityB(state, disentangled):
	g = G(state, disentangled)
	return numpy.sqrt(2.0 - 2.0 * numpy.sqrt(g))

def superfidelityB_normalized(state, disentangled):
	return superfidelityB(state, disentangled) * __1_over_bures_max[len(state)]

def superfidelityC(state, disentangled):
	g = G(state, disentangled)
	return numpy.sqrt(1.0 - g)

def superfidelityC_normalized(state, disentangled):
	return superfidelityC(state, disentangled) * __1_over_bures_max[len(state)]

# ---------------------------------------------------------------------------------------
# Subfidelity

def E(A, B):
	trAB = numpy.trace(A*B)
	return trAB + numpy.sqrt(2.0 * (trAB*trAB - numpy.trace(A*B*A*B)))

def subfidelity(state, disentangled):
	return numpy.sqrt(2.0 - 2.0 * abs(E(state, disentangled)))

def subfidelity_normalized(state, disentangled):
	return subfidelity(state, disentangled) * __1_over_bures_max[len(state)]

# ---------------------------------------------------------------------------------------
# Entanglement of formation

def concurrence(state):
	M = state * __sy_otimes_sy * state.conjugate() * __sy_otimes_sy
	singular_values = list(numpy.sqrt(numpy.linalg.eig(M)[0]))
	max_singular_value = max(singular_values)
	c = max([0.0, 2*max_singular_value - sum(singular_values)])
	x = 0.5 * (1.0 + numpy.sqrt(1.0 - c*c))
	if x == 0.0 or x == 1:
		return 0.0
	H = -x * numpy.log2(x) - (1.0 - x) * numpy.log2(1.0 - x)
	return abs(H)

##########################################################################################

# Functions used to check if distance can be calculated for given state

def __always_ok(state):
	return True

def __is_reversable(state):
	return numpy.linalg.det(state) != 0.0


# A dictionary for convenience
get = {
		'entropy':         (entropy,                    __is_reversable),
		'Hilbert-Schmidt': (hilbert_schmidt_normalized, __always_ok),
		'Bures':           (bures_normalized,           __is_reversable),
		'superfidelity':   (superfidelityB_normalized,  __always_ok),
		'superfidelityB':  (superfidelityB_normalized,  __always_ok),
		'superfidelityC':  (superfidelityC_normalized,  __always_ok),
		'subfidelity':     (subfidelity_normalized,     __always_ok),
		'concurrence':     (concurrence,                __always_ok)
	}

