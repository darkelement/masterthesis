# -*- coding: utf-8 -*-

"""Algorithms used for finding best fitting states"""

import sys

import scipy.optimize as optimize
import threading

import disentangled

# ---------------------------------------------------------------------------------------
# Straight

def straight(data, verbose):
	"""Function used when calculating measure is straight forward"""

	data.save([data.measure_fun(state) for state in data.base_states])

# ---------------------------------------------------------------------------------------
# Genetic

def genetic(data, verbose):
	"""Genetic algorithm"""

	print '\nGenetic algorithm:'
	print '\tevolution multiplier = %f' % data.evol_multiplier
	print '\tgenerations = %i' % (data.generations-1)
	print '\tpopulation = %i' % data.population
	print '\nCreating random states...'
	generations = [[disentangled.State.init_random(data.state_gen.dims, diff=0.1)
	                                  for j in range(0, data.population)] for x in data.X]
	closest_states = [disentangled.State.init_random(data.state_gen.dims, diff=0.1) for x in data.X]

	print 'Searching for closest state, please wait...'
	for g in range(0, data.generations):
		print '\033[1m%i\033[0m' % g,
		sys.stdout.flush()

		E, S = [], []
		for s in range(0, len(data.X)):
			# Calculate distances
			states = {data.measure_fun(data.base_states[s], state.get_matrix()):
					  state for state in generations[s]}
			dists = states.keys()
			dists.sort()
			minimal = dists[0]

			# Save closest state
			# (if it's the same as in previous genetarion, modify evolution parameter)
			if closest_states[s] == states[minimal]:
				for st in generations[s]:
					st.change_evolution_parameter(data.evol_multiplier)
			closest_states[s] = states[minimal]

			if verbose:
				print closest_states[s].diff,
				sys.stdout.flush()

			# Append plot data
			E.append(minimal)
			S.append(closest_states[s])

			# Generate new generation
			nextgen = [closest_states[s]]
			if s != 0:             nextgen.append(closest_states[s-1].copy())
			if s != len(data.X)-1: nextgen.append(closest_states[s+1].copy())

			for i in range(1, int(0.5*len(states)-4)):
				nextgen.append(disentangled.State.init_crossover(
						states[dists[i]], states[dists[i-1]]
					))
				#nextgen[-1].mutation()
				nextgen.append(states[dists[i]])
				nextgen[-1].mutation()

			while len(nextgen) < data.population:
				nextgen.append(disentangled.State.init_crossover(
						closest_states[s], states[dists[1]]
					))
				nextgen[-1].mutation()

			generations[s] = nextgen

		data.save(E, S)
		print

	return closest_states

# ---------------------------------------------------------------------------------------
# Numeric

def numeric(X, base_states, dist_fun, data, options, dims):
	Y, S = {}, {}
	vector = {x: [1 for i in range(0, (1+2*dims)*pow(4, dims))] for x in X}
	state_funs = {x: lambda v: dist_fun(base_states[x],
				  disentangled.State.init_from_vector(dims, v).get_matrix()) for x in X}
	
	def target(x):
		vector[x], fopt = optimize.fmin(state_funs[x], vector[x],
		                                maxfun=1e100, maxiter=1e100)
		S[x] = disentangled.State.init_from_vector(dims, vector[x])
		Y[x] = fopt

	T = [threading.Thread(target=target, args=(x,)) for x in X]
	for t in T: t.start()
	for t in T:
		t.join()
		print 'Joined'

	data.save(X, [Y[k] for k in list(Y)], [S[k] for k in list(S)])



# ---------------------------------------------------------------------------------------
# Hybrid

def hybrid(X, base_states, dist_fun, data, options, dims):
	pass

# ---------------------------------------------------------------------------------------

get = {
		'straight': straight,
		'genetic': genetic,
		'numeric': numeric,
		'hybrid': hybrid
	}
