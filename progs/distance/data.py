# -*- coding: utf-8 -*-

import os
import os.path
import json
import numpy

from quantum import *

class ReadData:
	default = {
			'state_name':      'Werner',
			'measure_name':    'entropy',
			'interfix':        'current',
			'algorithm':       'genetic',
			'population':      100,
			'generations':     400,
			'points':          11,
			'noise':           0.0,
			'evol_multiplier': 0.95
		}

	def __init__(self, config_file, directory):
		self.path = os.path.abspath(directory)
		self.config_file = os.path.join(self.path, config_file)

		if not os.path.isfile(self.config_file):
			raise Exception('Config file "%s" does not exist!' % self.config_file)

		with open(self.config_file, 'r') as f:
			conf = json.load(f)

		# Load defaults
		for attr in self.default.keys():
			self.__set(conf, attr)

		# Load state generator
		if self.state_name in states.get.keys():
			self.state_gen = states.get[self.state_name]
		else:
			raise Exception('State "%s" not found!' % self.state_name)

		if self.noise != 0.0:
			self.state_gen = states.Noised(self.state_gen, self.noise)

		# Check algorithm
		if self.algorithm not in algorithms.get.keys():
			raise Exception('Algorithm "%s" not found!' % self.algorithm)

		# Load measure
		if self.measure_name in measures.get.keys():
			self.measure_fun   = measures.get[self.measure_name][0]
			self.measure_check = measures.get[self.measure_name][1]
		else:
			raise Exception('Measure "%s" not found!' % self.measure_name)

		# Load borders
		try: self.a = conf['a']
		except KeyError: self.a = self.state_gen.a

		try: self.b = conf['b']
		except KeyError: self.b = self.state_gen.b

		try: self.param = conf['param']
		except KeyError: self.param = self.state_gen.param

		self.generations += 1

	def __set(self, d, attr):
		if attr in d:
			setattr(self, attr, d[attr])
		else:
			setattr(self, attr, self.default[attr])


class Data(ReadData):
	"""Save data for later use"""

	def __init__(self, config_file, directory):
		ReadData.__init__(self, config_file, directory)

		# Prepare files
		namebase = '%s.%s.%s' % (self.state_name, self.measure_name, self.interfix)
		self.filename_config  = os.path.join(self.path, '%s.conf.json' % namebase)
		self.filename_results = os.path.join(self.path, '%s.results.dat' % namebase)
		self.filename_states  = os.path.join(self.path, '%s.states.json' % namebase)

		if self.config_file != self.filename_config:
			if os.path.exists(self.filename_config):
				raise Exception('Config file has wrong name and can not be '
				                'moved to "%s"' % self.filename_config)
			else:
				print 'Config file has wrong name!'
				print 'Copying "%s" to "%s"' % (self.config_file, self.filename_config)
				open(self.filename_config, "w").write(open(self.config_file).read())

		for fname in (self.filename_results, self.filename_states):
			if os.path.isfile(fname):
				ans = raw_input('"%s" does already exist! Remove it? [y/N] ' \
				    % os.path.basename(fname))
				if ans in ('y', 'Y'):
					os.remove(fname)
				else:
					raise Exception('File exists!')

		# Generate list of states
		self.base_states = list()
		self.X = list()

		for x in numpy.linspace(self.a, self.b, self.points):
			state = self.state_gen(x)

			# Accept state only if distance can be counted
			if self.measure_check(state.matrix):
				self.base_states.append(state.matrix)
				self.X.append(x)

			else:
				print '\033[1mWarning\033[0m: state for x=%f disaccepted' % x

		self.save(self.X)


	def save(self, Y, S=None):
		# save entanglement
		with open(self.filename_results, 'a') as f:
			for i in range(0, len(Y)):
				f.write("%.12f\t" % Y[i])
			f.write('\n')

		# return if no state matrix to save
		if S == None: return

		# save states
		D = {x: s.dict() for x, s in zip(self.X, S)}
		with open(self.filename_states, 'w') as f:
			f.write(json.dumps(D))

