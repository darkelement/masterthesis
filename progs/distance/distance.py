#!/usr/bin/python2
# -*- coding: utf-8 -*-

import sys, os
import time, datetime
from argparse import ArgumentParser

sys.path.append('..')
os.system('rm -f *.pyc')

# Parse arguments
argparser = ArgumentParser()
argparser.add_argument('-c', '--config', type=str, required=True,
          help='path to config file')
argparser.add_argument('-d', '--directory', default='output', type=str,
          help='output directory')
argparser.add_argument('-v', '--verbose', default=False, action='store_true',
          help='print additional information?')
options = argparser.parse_args(sys.argv[1:])

# Import numerical libraries
try:
	import numpy
	import scipy
	import scipy.linalg
	import matplotlib

except ImportError as e:
	print e.message
	exit()

# Import programs libraries
import data
from quantum import *

# ---------------------------------------------------------------------------------------

try:
	data = data.Data(options.config, options.directory)
except Exception as e:
	print '\033[1mError parsing configuration:\033[0m', e
	exit(1)
	
print 'Calculations for'
print '\t* state:    "%s"' % data.state_name
print '\t* measure:  "%s"' % data.measure_name
print '\t* interfix: "%s"\n' % data.interfix

# If no states were accepted print error and exit
if len(data.base_states) < 2:
	print "\033[1mToo few acceptable %s states in range [%f, %f]\033[0m" \
	                                    % (data.state_name, data.a, data.b)
	exit(2)

print 'For x in ', data.X

start_time = time.time()

# Main part of program
try:
	algorithms.get[data.algorithm](data, options.verbose)

except KeyboardInterrupt:
	print '\n\nInterrupted!'

else:
	print '\n\nSuccess!'

finally:
	print '\nTime elapsed: %s\n' % str(datetime.timedelta(0, int(time.time()-start_time)))

