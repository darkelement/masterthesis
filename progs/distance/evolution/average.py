#!/usr/bin/python2

import sys

import numpy

def average(l):
	return sum(l) / len(l)

number = int(sys.argv[1])
filenames = sys.argv[2:]

results = [numpy.loadtxt(filename) for filename in filenames]
averages = [[average(step[:number]) for step in simulation] for simulation in results]

for av in range(0, len(averages[0])):
	for sim in range(0, len(averages)):
		print '%.14f\t' % averages[sim][av],
	print

