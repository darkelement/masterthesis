#!/usr/bin/python2
# -*- coding: utf-8 -*-

import os
import sys
import string
import argparse
import fnmatch
import json

import numpy
import matplotlib; matplotlib.use('cairo')
import matplotlib.pyplot as plt

sys.path.append('..')

import data
from quantum import disentangled

# Parse arguments
argparser = argparse.ArgumentParser()
argparser.add_argument('-p', '--prefix', default=None, type=str,
                       help='Use only files with this prefix')
argparser.add_argument('-s', '--state', default=None, type=str,
                       help='Use only this state')
argparser.add_argument('-m', '--measure', default=None, type=str,
                       help='Use only this measure')
argparser.add_argument('-i', '--interfix', default=None, type=str,
                       help='Use only this interfix')
argparser.add_argument('-d', '--directory', default='output', type=str,
                       help='working directory')
argparser.add_argument('--ylim', default=None, type=float,
                       help='Limit on range of Y-axis of entanglement plot')
argparser.add_argument('-n', '--nolegend', default=False, action='store_true',
                       help='Don\'t draw legend on evolution plot')
argparser.add_argument('--log', default=False, action='store_true',
                       help='Use logarithmic on evolution plot')
argparser.add_argument('-l', '--legendpos', default='upper right', type=str,
                       help='placement of legend on entanglement plot',
                       choices=['upper right', 'upper left', 'lower left', 'lower right',
                       'right', 'center left', 'center right', 'lower center',
                       'upper center', 'center'])
options = argparser.parse_args(sys.argv[1:])

##########################################################################################
# Filter files

path = os.path.abspath(options.directory)
if not os.path.exists(path):
	print 'Path "%s" does not exist!' % path
	exit(1)

os.chdir(path)

prefixes = []
for f in os.listdir('.'):
	if not fnmatch.fnmatch(f, '*.conf.json'): continue

	if options.prefix != None:
		if not fnmatch.fnmatch(f, '%s.*' % options.prefix): continue

	slices = f.split('.')

	if options.state    != None and slices[0] != options.state:    continue
	if options.measure  != None and slices[1] != options.measure:  continue
	if options.interfix != None and slices[2] != options.interfix: continue

	prefix = string.join(slices[:3], '.')

	for e in ('results.dat', 'states.json'):
		filename = '%s.%s' % (prefix, e)
		if not os.path.isfile(filename):
			print '\033[1m File "%s" not found!\033[0m' % filename
			continue

	prefixes.append(string.join(slices[:3], '.'))

if len(prefixes) == 0:
	print 'No mathing files!'
	exit(1)

else:
	prefixes = sorted(prefixes)
	print '\n * Found files with prefixes: %s\n' % repr(prefixes)

figsize=(11.7, 7.5)

##########################################################################################
# Ploting all data

# Prepare combined entanglement plots
fig_ents = plt.figure(figsize=figsize)
ax_ents = fig_ents.add_subplot(111)

params = []

for p in prefixes:

	# ------------------------------------------------------------------------------------

	filename_conf = '%s.conf.json' % p
	filename_results = '%s.results.dat' % p
	filename_states = '%s.states.json' % p
	filename_summary = '%s.summary.tex' % p

	print ' + using "%s"' % filename_conf

	# Load data
	conf = data.ReadData(filename_conf, '.')
	results = numpy.loadtxt(filename_results)
	X = results[0]
	results = results[1:]

	try:
		with open(filename_states, 'r') as f:
			states = json.load(f)
	except:
		states = None

	params.append(conf.param)

	state_name = conf.state_name if conf.noise == 0.0 \
				 else '%s [with noise - %e]' % (conf.state_name, conf.noise)

	# ------------------------------------------------------------------------------------
	# Entanglement

	# Prepare plots
	fig_ent = plt.figure(figsize=figsize)
	ax_ent = fig_ent.add_subplot(111)

	# Plot data
	for ax in (ax_ent, ax_ents):
		ax.plot(X, results[-1], 'o-', markersize=5.0, markeredgecolor=None,
		        label='state: %s, measure: %s' % (conf.state_name, conf.measure_name))

	# Draw legend and labels
	ax_ent.legend(loc=options.legendpos)

	if options.ylim != None:
		ax_ent.set_ylim(0.0, options.ylim)

	ax_ent.set_title('Entanglement')
	ax_ent.set_xlabel(conf.param)
	ax_ent.set_ylabel('Entanglement')

	# Save figure
	output_ent = '%s.entanglement.eps' % p
	print '   > saving entanglement to file "%s"' % output_ent
	fig_ent.savefig(output_ent)

	# -----------------------------------------------------------------------------------

	if len(results) < 2: print; continue

	# -----------------------------------------------------------------------------------
	# Evolution

	# Prepare plots
	fig_evol = plt.figure(figsize=figsize)
	ax_evol = fig_evol.add_subplot(111)

	# Plot data
	for i in range(0, len(results[0])):
		v = results[:,i]
		generations = len(v)
		if isinstance(conf.param, list):
			label = conf.param[i]
		else:
			label = '%s = %f' % (conf.param, float(X[i]))
		ax_evol.plot(numpy.arange(0, generations), v, '-', label=label)

	# Draw legend and labels
	if not options.nolegend:
		ax_evol.legend()

	if options.log:
		ax_evol.set_yscale('log')

	ax_evol.set_title('Evolution of %s (measure: %s)' % (state_name, conf.measure_name))
	ax_evol.set_xlabel('Generation')
	ax_evol.set_ylabel('Entanglement')

	output_evol = '%s.evolution.eps' % p
	print '   > saving evolution to file "%s"' % output_evol
	fig_evol.savefig(output_evol)

	# -----------------------------------------------------------------------------------
	# Summary

	with open(filename_summary, 'w') as f:
		f.write('\documentclass[a4paper]{article}\n\usepackage{amsmath}\n'
		        '\usepackage[landscape,a4paper,margin=1cm]{geometry}\n'
		        '\usepackage[pdftex]{color,graphicx}\n\\begin{document}\n\n'
		        '\section*{Summary of %s state after %i generations:}' % \
		        (state_name, generations))

		if states is not None:
			for x in sorted(states.keys()):
				f.write('\\begin{align*}\n& \mathrm{%s}^*(%5f)=\\\\\n'
				        '&\t%s\end{align*}\n\n' % (conf.state_name, float(x),
				        disentangled.State.init_from_dict(states[x]).latex()))

		f.write('\includegraphics[scale=0.9]{./{%s.entanglement}.eps}\\\\\n' % p)
		if len(results) > 1:
			f.write('\includegraphics[scale=0.9]{./{%s.evolution}.eps}\n' % p)
		f.write('\end{document}')

	print '   > compiling summary "%s"' % filename_summary
	r = os.system('pdflatex -halt-on-error %s > /dev/null' % filename_summary)
	if r != 0:
		os.system('pdflatex -halt-on-error %s' % filename_summary)
	os.system('rm -f *.aux *.log *-eps-converted-to.pdf')

	# -----------------------------------------------------------------------------------

	print

# Save combined entanglement
ax_ents.legend(loc=options.legendpos)

if options.ylim != None:
	ax_ents.set_ylim(0.0, options.ylim)

ax_ents.set_title('Entanglement')
ax_ents.set_xlabel(', '.join(params))
ax_ents.set_ylabel('Entanglement')

output_ents = '%s.entanglement.eps' % (p if len(prefixes) == 1 else str(prefixes))
print ' > saving entanglement to file "%s"\n' % output_ents
fig_ents.savefig(output_ents)

# THE END
