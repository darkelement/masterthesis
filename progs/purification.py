#!/usr/bin/python2

import math
from sympy import *
from sympy.physics.quantum import TensorProduct as T
from sympy.functions import conjugate

def simplify_vector(vector):
	return Matrix([simplify(v) for v in vector])

Phi_p = Matrix([1, 0, 0, 1])
Phi_m = Matrix([1, 0, 0,-1])
Psi_p = Matrix([0, 1, 1, 0])
Psi_m = Matrix([0, 1,-1, 0])

R = Matrix([[ 1, -I], [-I,  1]]) / sqrt(2)
#R = Matrix([[1,  1], [1, -1]]) / sqrt(2)
R1 = R.inv()
RT = T(R, R1)

a, b, c, d = symbols('a,b,c,d')
e, f, g, h = symbols('e,f,g,h')

state = sqrt(a)*Phi_p + sqrt(b)*Psi_m + sqrt(c)*Psi_p + sqrt(d)*Phi_m

rotated = simplify_vector(RT * state)
result = simplify_vector(T(rotated, rotated))

M_CNOT = Matrix([[1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0],
                 [0,1,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0],
                 [0,0,1,0, 0,0,0,0, 0,0,0,0, 0,0,0,0],
                 [0,0,0,1, 0,0,0,0, 0,0,0,0, 0,0,0,0],

                 [0,0,0,0, 0,1,0,0, 0,0,0,0, 0,0,0,0],
                 [0,0,0,0, 1,0,0,0, 0,0,0,0, 0,0,0,0],
                 [0,0,0,0, 0,0,0,1, 0,0,0,0, 0,0,0,0],
                 [0,0,0,0, 0,0,1,0, 0,0,0,0, 0,0,0,0],

                 [0,0,0,0, 0,0,0,0, 0,0,1,0, 0,0,0,0],
                 [0,0,0,0, 0,0,0,0, 0,0,0,1, 0,0,0,0],
                 [0,0,0,0, 0,0,0,0, 1,0,0,0, 0,0,0,0],
                 [0,0,0,0, 0,0,0,0, 0,1,0,0, 0,0,0,0],

                 [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,1],
                 [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,1,0],
                 [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,1,0,0],
                 [0,0,0,0, 0,0,0,0, 0,0,0,0, 1,0,0,0]])

result = simplify_vector(M_CNOT * result)

S = Matrix([[1, 0, 0, 1],
            [0, 1, 1, 0],
            [0,-1, 1, 0],
            [1, 0, 0,-1]])

SS = T(S, S)
SS1 = SS.inv()

result = simplify_vector(SS1 * result)

for b in ((0b0000, 0b0011), (0b0100, 0b0111), (0b1000, 0b1011), (0b1100, 0b1111)):
	print
	pprint(simplify(result[b[0]] * conjugate(result[b[0]]) + result[b[1]] * conjugate(result[b[1]])))

#########################################################################

import numpy
import pylab

def next(L):
	A, B, C, D = L[0], L[1], L[2], L[3]
	N = (A+B) * (A+B) + (C+D) * (C+D)
	return [(A*A + B*B)/N, 2*C*D/N, (C*C + D*D)/N, 2*A*B/N]

S = [i for i in range(0, 11)]
F = numpy.arange(0.0, 1.0001, 0.01)

V = [[f, (1-f)/3.0, (1-f)/3.0, (1-f)/3.0] for f in F]
Z = []
for s in S:
	Z.append([v[0] for v in V])
	V = [next(v) for v in V]


pylab.xlabel('Fidelity')
pylab.ylabel('Number of steps')
pylab.xticks(numpy.linspace(0.0, 1.0, 5))
pylab.yticks(numpy.linspace(0, 10, 11))

pylab.grid()
pylab.contourf(F, S, Z, numpy.linspace(0.0, 1.0, 64))
pylab.colorbar(ticks=numpy.linspace(0.0, 1.0, 5), orientation='horizontal')

pylab.savefig('purification.eps')


