import wx
import wx.glcanvas

from OpenGL.GL import *
from OpenGL.GLU import *

import geometry
import scale

class VisualizatorGL(wx.glcanvas.GLCanvas):
	def __init__(self, parent, L, details):
		wx.glcanvas.GLCanvas.__init__(self, parent, wx.ID_ANY,
		                              attribList = [wx.glcanvas.WX_GL_RGBA,
		                              wx.glcanvas.WX_GL_DOUBLEBUFFER,
		                              wx.glcanvas.WX_GL_DEPTH_SIZE, 24])

		self.width  = 600
		self.height = 500
		self.phi = 0
		self.theta = 0
		self.distance = 5.0
		self.ready = False
		self.timer = wx.Timer(self)

		self.L = L
		self.details = details

		# Connect events
		self.Bind(wx.EVT_PAINT, self.on_paint)
		self.Bind(wx.EVT_SIZE, self.on_size)
		self.Bind(wx.EVT_CHAR, self.on_key)
		self.Bind(wx.EVT_TIMER, self.on_timer)


	def rotation(self, rotate):
		if rotate:
			self.timer.Start(20.0)
		else:
			self.timer.Stop()

	def rotate(self, phi, theta):
		self.theta = (self.theta + theta) % 360
		self.phi = (self.phi + phi) % 360
		self.Refresh()

	def zoom(self, zoom):
		self.distance += zoom
		self._set_viewpoint()
		self.Refresh()

	def on_paint(self, event):
		self.SetCurrent()
		glClearColor(0.9, 0.9, 0.9, 1)

		if not self.ready:
			self._init_gl()
			self.load_data()
			self.ready = True

		self._clear()
		self._set_projection()
		self._set_viewpoint()
		self._draw()
		self.SwapBuffers()

	def on_size(self, event):
		self.width, self.height = self.GetSizeTuple()
		print self.width, self.height

	def on_key(self, event):
		code = event.GetKeyCode()

		if code == wx.WXK_ESCAPE:
			self.GetParent().Close()

		elif code == wx.WXK_SPACE:
			 self.rotation(not self.timer.IsRunning())

		elif code == wx.WXK_UP:
			self.rotate(0, 1)

		elif code == wx.WXK_DOWN:
			self.rotate(0, -1)

		elif code == wx.WXK_RIGHT:
			self.rotate(1, 0)

		elif code == wx.WXK_LEFT:
			self.rotate(-1, 0)

		elif code == wx.WXK_NUMPAD_ADD or code == wx.WXK_ADD or code == 45:
			self.zoom(0.5)

		elif code == wx.WXK_NUMPAD_SUBTRACT or code == wx.WXK_SUBTRACT or code == 43:
			self.zoom(-0.5)

	def on_timer(self, event):
		self.phi = (self.phi + 1) % 360
		self.Refresh()


	def load_data(self):
#		figure = geometry.StructureGenerator.Icosahedron()
#
#		glNewList(1, GL_COMPILE)
#		glBegin(GL_TRIANGLES)
#
#		for r in figure.get_triangles_and_normal():
#			triangle, normal = r[0], r[1]
#
#			for i in triangle:
#				p, l, c = figure.get_vertex(i)
#
#				glColor4f(c[0], c[1], c[2], 0.5)
#				apply(glNormal3f, normal)
#				glVertex3f(l*p[0], l*p[1], l*p[2])
#
#		glEnd()
#		glEndList()
		figure = geometry.StructureGenerator.Sphere(4)

		glNewList(1, GL_COMPILE)
		glBegin(GL_LINES);

		for i, p in enumerate(figure.get_vertices()):
			l, c = scale.scale_bloch(p, self.L)
			figure.set_vertex(i, l, c)

		for r in figure.get_triangles_and_normal():
			triangle, normal = r[0], r[1]

			p1, l1, c1 = figure.get_vertex(triangle[0])
			p2, l2, c2 = figure.get_vertex(triangle[1])
			p3, l3, c3 = figure.get_vertex(triangle[2])

			apply(glNormal3f, normal)

			glColor4f(c1[0], c1[1], c1[2], 0.5)
			glVertex3f(l1*p1[0], l1*p1[1], l1*p1[2])
			glColor4f(c2[0], c2[1], c2[2], 0.5)
			glVertex3f(l2*p2[0], l2*p2[1], l2*p2[2])

			glColor4f(c1[0], c1[1], c1[2], 0.5)
			glVertex3f(l1*p1[0], l1*p1[1], l1*p1[2])
			glColor4f(c3[0], c3[1], c3[2], 0.5)
			glVertex3f(l3*p3[0], l3*p3[1], l3*p3[2])

			glColor4f(c3[0], c3[1], c3[2], 0.5)
			glVertex3f(l3*p3[0], l3*p3[1], l3*p3[2])
			glColor4f(c2[0], c2[1], c2[2], 0.5)
			glVertex3f(l2*p2[0], l2*p2[1], l2*p2[2])

		glEnd()
		glEndList()

		if len(self.L[0]) > 3:
			figure = geometry.StructureGenerator.Sphere(self.details)

			scale_function = scale.scale_entangled if len(self.L[0]) > 3 \
							 else scale.scale_bloch

			glNewList(2, GL_COMPILE)
			glBegin(GL_TRIANGLES)

			for i, p in enumerate(figure.get_vertices()):
				l, c = scale_function(p, self.L)
				figure.set_vertex(i, l, c)

			for r in figure.get_triangles_and_normal():
				triangle, normal = r[0], r[1]

				for i in triangle:
					p, l, c = figure.get_vertex(i)

					glColor4f(c[0], c[1], c[2], 0.5)
					apply(glNormal3f, normal)
					glVertex3f(l*p[0], l*p[1], l*p[2])

			glEnd()
			glEndList()

	def _load_vertices(self):
		pass

	def _clear(self):
		glViewport(0, 0, self.width, self.height)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

	def _set_projection(self):
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(25.0, float(self.width) / float(self.height), 1.0, 20.0)
		glMatrixMode(GL_MODELVIEW)

	def _set_viewpoint(self):
		glLoadIdentity()
		gluLookAt(0.0, 2.0, self.distance, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

	def _load_shader(self, type, source):
		shader = glCreateShader(type)
		glShaderSource(shader, source)
		glCompileShader(shader)
	
		if glGetShaderiv(shader,GL_COMPILE_STATUS) != 1:
			raise Exception('Couldn\'t compile shader\nShader compilation Log:\n%s\n' %
			                glGetShaderInfoLog(shader))

		return shader

	def _init_gl(self):
		glEnable(GL_DEPTH_TEST)
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT)
		glDepthFunc(GL_LESS)

		#glEnable(GL_BLEND)
		#glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

		vertex_shader_file = open('./vertex.glsl', 'r')
		vertex_shader_source = vertex_shader_file.read()
		vertex_shader_file.close()

		fragment_shader_file = open('./fragment.glsl', 'r')
		fragment_shader_source = fragment_shader_file.read()
		fragment_shader_file.close()

		vertex_shader = self._load_shader(GL_VERTEX_SHADER, vertex_shader_source) 
		fragment_shader = self._load_shader(GL_FRAGMENT_SHADER, fragment_shader_source) 

		program = glCreateProgram()
		glAttachShader(program, vertex_shader)
		glAttachShader(program, fragment_shader)
		glLinkProgram(program)

		try:
			glUseProgram(program)
		except OpenGL.error.GLError:
			print glGetProgramInfoLog(program)
			raise

		glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, [1.0, -1.0, -1.0, 1.0])

	def _draw (self):
		glPushMatrix()
		glRotatef(self.phi, 0, 1, 0)
		glRotatef(self.theta, 1, 0, 0)
		glCallList(1)
		if len(self.L[0]) > 3:
			glCallList(2)
		glPopMatrix()


