import numpy

import geometry
from quantum import measures

epsilon = 0.0000001
s3 = numpy.sqrt(3.0);
rs3 = 1.0 / s3

#########################################################################################

def __calc_nnn(n):
	"""Returns scalar product of 'n' and star product of 'n' and 'n'"""
	n1 = n[0]; n2 = n[1]; n3 = n[2]; n4 = n[3]
	n5 = n[4]; n6 = n[5]; n7 = n[6]; n8 = n[7]
	n12 = n1*n1; n22 = n2*n2; n32 = n3*n3; n42 = n4*n4
	n52 = n5*n5; n62 = n6*n6; n72 = n7*n7; n82 = n8*n8
	return 3.0*n8*(n12 + n22 + n32 - 0.5*(n42 + n52 + n62 + n72) - n82/3.0) \
	     + 3.0*s3*(n1*n4*n6 + n1*n5*n7 + n2*n5*n6 - n2*n4*n7) \
	     + 1.5*s3*n3*(n42 + n52 - n62 - n72)

def __is_ok(z):
	if z.imag < epsilon and z.real <= 1.0 and z.real >= 0:
		return True
	return False

########################################################################################

def is_state(state):
	result = [True, [0.0,0.0,0.0]]

	if abs(numpy.trace(state * state)) > 1.0:
		return [False, [0.0, 0.0, 0.0]]
	
	if (state != state.H).any():
		return [False, [1.0, 1.0, 1.0]]
	
	r = 0
	eigenvalues = numpy.linalg.eig(state)[0]
	for i, e in enumerate(eigenvalues):
		if e < 0.0:
			result[0] = False
			try:
				result[1][i] += 1
			except:
				pass

	return result

def is_entangled(state):
	return measures.concurrence(state) == 0.0

def get_state(s, P, L):
	level = len(L[0])
	result = numpy.identity(level, dtype=numpy.dtype('complex'))
	for p, l in zip(P, L):
		result += s * p * l
	return numpy.matrix(result) / level

########################################################################################

def scale_bloch_fast(P, D):
	N = [0 for l in L]
	for p, d in zip(P, D):
		N[d] = p
	nnn = calc_nnn(N)

	if (nnn == 0.0):
		return [rs3, (0.5, 1.0, 0.0)]
	elif (nnn == 1.0):
		return [1.0, (1.0, 0.0, 0.0)]
	elif (nnn == -1.0):
		return [0.5, (0.0, 1.0, 0.0)]

	# frequently used variables
	nnn2 = nnn * nnn
	a = -2.0 * nnn;
	h1 = -1.0 / (      a)
	h3 = -1.0 / (3.0 * a)

	e = -108.0 * nnn2 + 54.0;
	f = numpy.sqrt(11664.0 * nnn2 * (nnn2 - 1.0) + 0j)
	sp = numpy.power(0.5 * (e + f), 1.0/3.0)
	sm = numpy.power(0.5 * (e - f), 1.0/3.0)

	x1 = h1 + h3 * (sp + sm)

	h6p  = (1.0 + 1j*s3) / (6.0 * a)
	h6m  = (1.0 - 1j*s3) / (6.0 * a)

	x2 = h1 + h6p*sp + h6m*sm
	x3 = h1 + h6m*sp + h6p*sm

	if is_ok(x1):
		print "error: x1 ok"
		exit()

	if is_ok(x2):
		result = x2.real
	elif is_ok(x3):
		result = x3.real
	else:
		print "error: no root found ok"
		exit()

	if result < 0.52:
		color = (0.0, 1.0, 0.0)
	elif result < 0.6:
		color = (0.5, 1.0, 0.0)
	elif result < 0.98:
		color = (1.0, 1.0, 0.0)
	else:
		color = (1.0, 0.0, 0.0)

	return [result, color]

def scale_bloch(P, D):
	a = m = 1.0
	b = r = 0.0
	for i in range(0, 20):
		m = (a + b) / 2.0
		if not is_state(get_state(m, P, D))[0]:
			r = a = m
		else:
			r = b = m

	if r < 0.52:
		color = (0.0, 1.0, 0.0)
	elif r < 0.6:
		color = (0.5, 1.0, 0.0)
	elif r < 0.95:
		color = (1.0, 1.0, 0.0)
	else:
		color = (1.0, 0.0, 0.0)

	return [r, color]
	return [r, is_state(get_state(r, P, D))[1]]

def scale_entangled(P, D):
	a = m = scale_bloch(P, D)[0]
	b = 0.0
	for i in range(0, 10):
		if is_entangled(get_state(m, P, D)):
			b = m
		else:
			a = m
		m = (a + b) / 2.0

	return [m, [1-m,0,m]]

