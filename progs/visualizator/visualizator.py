#!/usr/bin/python2

import sys
from argparse import ArgumentParser 

sys.path.append('..')

from quantum import states

argparser = ArgumentParser()
argparser.add_argument('-l', '--level', default=3, type=int,
          help='systems level', choices=[3,4])
argparser.add_argument('-s', '--states', default='', type=str,
          help='coma-separated list of states')
argparser.add_argument('-o', '--output', default='', type=str,
          help='output file name')
argparser.add_argument('--detail', default=0, type=int,
          help='how much details should be drawed')
argparser.add_argument('--nolabels', default=False, action='store_true',
          help='should labels be drawed on plots')
argparser.add_argument('--entangled', default=False, action='store_true',
          help='should entagled area be drawed')
argparser.add_argument('--family', default=False, action='store_true',
          help='should family be drawed')
argparser.add_argument('--size', default=5.0, type=float,
          help='size of drawed plots in inches')
options = argparser.parse_args(sys.argv[1:])

S = options.states.split(',')
dimention = len(S)

output = '{0}.eps'.format(options.states) if options.output == '' \
                                          else options.output

try:
	L = [states.traceless[str(options.level)][s] for s in S] 

except Exception as e:
	print 'At least one of requested states do not exist!'
	exit()

detail = options.detail
if detail == 0:
	if dimention == 2:
		detail = 512
	else:
		detail = 3


if __name__ == '__main__':
	if dimention == 2:
		import visualizator_2d
		vis = visualizator_2d.Visualizator(detail, options.size)
		xlabel, ylabel = S[0], S[1]
		if options.nolabels:
			xlabel, ylabel = '', ''
		vis.generate_bloch(output, L, xlabel, ylabel, options.entangled, options.family)

	else:
		import wx
		import visualizator_gl

		app = wx.App(redirect=False)
		window = wx.Frame(parent=None, id=wx.ID_ANY,
						  title='Bloch set visualizator',
						  pos=wx.DefaultPosition, size=(800, 600))
		vis = visualizator_gl.VisualizatorGL(window, L, detail)

		window.Show()
		app.SetTopWindow(window)
		app.MainLoop()


