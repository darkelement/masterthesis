import math
import numpy

################################################################################
# Polyhedron data container

class Polyhedron:
	def __init__(self, vertices, triangles, colors=None):
		self.vertices = list() # ordered list of points
		self.lengths = list() # ordered list of lenghts
		self.triangles = triangles # set of triangles defined as tuples of indices

		if colors is None:
			self.colors = [[1.0, 0.0, 0.0] for i in vertices]
		else:
			self.colors = colors

		for vertex in vertices:
			l = math.sqrt(sum([v*v for v in vertex]))
			self.lengths.append(l)
			self.vertices.append(tuple([v/l for v in vertex]))

	def get_vertices(self):
		for vertex in self.vertices:
			yield vertex

	def get_triangles(self):
		for triangle in self.triangles:
			yield triangle

	def get_triangles_and_normal(self):
		for triangle in self.triangles:
			v = [self.lengths[i] * numpy.array(self.vertices[i])
			     for i in triangle]
			c = numpy.cross(v[0]-v[1], v[0]-v[2])
			if numpy.vdot(c, v[2]) < 0:
				c = -c
			yield triangle, c / numpy.linalg.norm(c)

	def get_vertex(self, index):
		return self.vertices[index], self.lengths[index], self.colors[index]

	def set_vertex(self, index, length, color=None):
		self.lengths[index] = length
		if color is not None:
			self.colors[index] = color


################################################################################
# Structures generator class

class StructureGenerator:

	# --------------------------------------------------------------------------

	@staticmethod
	def Circle(n):
		"""Circle generation function"""
		angles = [2.0 * math.pi * i / n for i in range(0, n+1)]
		return {'x': [math.cos(a) for a in angles],
		        'y': [math.sin(a) for a in angles]}

	# --------------------------------------------------------------------------

	@staticmethod
	def Icosahedron():
		"""Icosahedron generation function"""
		f = (math.sqrt(5.0) + 1.0) / 2.0
		b = math.sqrt(2.0 / (5.0 + math.sqrt(5.0)))
		a = b * f

		return Polyhedron(
		         (( 0, b, a), ( 0, b,-a), ( 0,-b, a), ( 0,-b,-a),
		          ( a, 0, b), ( a, 0,-b), (-a, 0, b), (-a, 0,-b),
		          ( b, a, 0), ( b,-a, 0), (-b, a, 0), (-b,-a, 0)),
		     set(((0,  2, 4), (0,  2, 6), (1,  3,  5), (1,  3,  7),
		          (4,  5, 8), (4,  5, 9), (6,  7, 10), (6,  7, 11),
		          (8, 10, 0), (8, 10, 1), (9, 11,  2), (9, 11,  3),
		          (4,  8, 0), (5,  8, 1), (4,  9,  2), (5,  9,  3),
		          (6, 10, 0), (7, 10, 1), (6, 11,  2), (7, 11,  3)))
		    )

	# --------------------------------------------------------------------------

	@staticmethod
	def Sphere(n):
		"""Sphere generation function"""
		icosahedron = StructureGenerator.Icosahedron()
		vertices = icosahedron.vertices
		old_triangles = icosahedron.triangles
		indices = {v: i for i, v in enumerate(vertices)}

		def nm(v1, v2):
			vector = [(v1[i] + v2[i])/2.0 for i in range(0, len(v1))]
			length = math.sqrt(sum((v*v for v in vector)))
			return tuple([v / length for v in vector])

		def index(v):
			if not indices.has_key(v):
				indices[v] = len(vertices)
				vertices.append(v)

			return indices[v]

		for i in range(0, n):
			new_triangles = set()
			
			for t in old_triangles:
				v0 = nm(vertices[t[1]], vertices[t[2]])
				v1 = nm(vertices[t[0]], vertices[t[2]])
				v2 = nm(vertices[t[0]], vertices[t[1]])

				p0 = index(v0)
				p1 = index(v1)
				p2 = index(v2)

				new_triangles.update([(t[0], p1, p2)])
				new_triangles.update([(t[1], p0, p2)])
				new_triangles.update([(t[2], p0, p1)])
				new_triangles.update([(p0, p1, p2)])

			old_triangles = new_triangles

		return Polyhedron(vertices, new_triangles)


