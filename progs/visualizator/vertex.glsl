out vec4 C;
out vec3 N;
out vec3 V;
out float d;

void main(void) {
	C = gl_Color;
	N = gl_NormalMatrix * gl_Normal;
	V = gl_ModelViewMatrix * gl_Vertex;

	vec3 L = gl_LightSource[0].spotDirection;
	d = dot(normalize(N), L);


	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
