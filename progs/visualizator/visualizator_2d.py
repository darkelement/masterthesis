import math
import numpy
import matplotlib
matplotlib.use('cairo')

import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.lines import Line2D

import scale
from geometry import StructureGenerator
from quantum import *

matplotlib.rc('font', size=9)

s3 = math.sqrt(3.0)
rs2 = 1.0/math.sqrt(2.0)
rs6 = 1.0/math.sqrt(6.0)
rs3 = 1.0/s3

class Visualizator:
	def __init__(self, detail, size):
		self.detail = detail
		self.size = size

	def generate_bloch(self, output, L, xlabel, ylabel, entangled, family):
		fig = plt.figure(figsize=(self.size, self.size))
		ax = fig.add_subplot(111, aspect='equal')
		circle = StructureGenerator.Circle(self.detail)

		# Draw states
		X, Y = [], []
		for x, y in zip(circle['x'], circle['y']):
			r = scale.scale_bloch((x,y), L)[0]
			X.append(r*x)
			Y.append(r*y)
		ax.add_artist(Polygon(zip(X, Y), linewidth=2,
		              edgecolor=(0,0,1), facecolor=(0.5,0.5,1)))

		# Draw entangled
		if entangled:
			if len(L[0]) > 3:
				X, Y = [], []
				for x, y in zip(circle['x'], circle['y']):
					r = scale.scale_entangled((x,y), L)[0]
					X.append(r*x)
					Y.append(r*y)
				ax.add_artist(Polygon(zip(X, Y), linewidth=1,
							  edgecolor=(0,0,0), facecolor=(0.5,0.5,0.5)))

		# Draw family
		if family:
			x = 1 # numpy.trace(L[0]*L[0]) / 12.0
			y = 1 # numpy.trace(L[1]*L[1]) / 12.0
			ax.add_line(Line2D([x, 0], [0, y], linewidth=2, color=(1.0,0.5,0.0)))

		# Draw lines
		circle = StructureGenerator.Circle(64)
		X = numpy.array(circle['x'])
		Y = numpy.array(circle['y'])

		ax.plot(    X,     Y, 'r-')
		ax.plot(rs3*X, rs3*Y, 'y-')
		if len(L[0]) > 3:
			ax.plot(rs2*X, rs2*Y, 'y-')
			ax.plot(rs6*X, rs6*Y, 'y-')
			ax.plot(X/3.0, Y/3.0, 'g-')
		else:
			ax.plot(0.5*X, 0.5*Y, 'g-')

		ax.set_xlabel(xlabel)
		ax.set_ylabel(ylabel)
		ax.set_xlim([-1.1, 1.1])
		ax.set_ylim([-1.1, 1.1])

		# Save
		plt.savefig(output)

