in vec4 C;
in vec3 N;
in vec3 V;
in float d;

void main(void) {
	gl_FragColor = 0.7*C - 0.3*d*vec4(1.0, 1.0, 1.0, 0.0);
}
