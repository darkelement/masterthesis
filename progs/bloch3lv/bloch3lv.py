#!/usr/bin/python

import sys
import math

################################################################################
# Define icosahedron

f = (math.sqrt(5.0) + 1.0) / 2.0
b = math.sqrt(2.0 / (5.0 + math.sqrt(5.0)))
a = b * f

triangles = [
	(( 0, b, a), ( 0,-b, a), ( a, 0, b)),
	(( 0, b, a), ( 0,-b, a), (-a, 0, b)),
	(( 0, b,-a), ( 0,-b,-a), ( a, 0,-b)),
	(( 0, b,-a), ( 0,-b,-a), (-a, 0,-b)),
	(( a, 0, b), ( a, 0,-b), ( b, a, 0)),
	(( a, 0, b), ( a, 0,-b), ( b,-a, 0)),
	((-a, 0, b), (-a, 0,-b), (-b, a, 0)),
	((-a, 0, b), (-a, 0,-b), (-b,-a, 0)),
	(( b, a, 0), (-b, a, 0), ( 0, b, a)),
	(( b, a, 0), (-b, a, 0), ( 0, b,-a)),
	(( b,-a, 0), (-b,-a, 0), ( 0,-b, a)),
	(( b,-a, 0), (-b,-a, 0), ( 0,-b,-a)),
	(( a, 0, b), ( b, a, 0), ( 0, b, a)),
	(( a, 0,-b), ( b, a, 0), ( 0, b,-a)),
	(( a, 0, b), ( b,-a, 0), ( 0,-b, a)),
	(( a, 0,-b), ( b,-a, 0), ( 0,-b,-a)),
	((-a, 0, b), (-b, a, 0), ( 0, b, a)),
	((-a, 0,-b), (-b, a, 0), ( 0, b,-a)),
	((-a, 0, b), (-b,-a, 0), ( 0,-b, a)),
	((-a, 0,-b), (-b,-a, 0), ( 0,-b,-a)),
]

################################################################################
# Circle generation function

def gen_circle(n):
	points = []
	for i in range(0,n):
		angle = 2.0 * math.pi * i / n
		x = math.cos(angle)
		y = math.sin(angle)
		points.append((x, y))
	return points

################################################################################
# Sphere generation functions

def normaliz(vector):
	length = math.sqrt(sum((v*v for v in vector)))
	return tuple([v / length for v in vector])

def medium(p1, p2):
	return [(p1[i] + p2[i])/2.0 for i in range(0, len(p1))]

def divide_triangles(triangles):
	new_triangles = []

	for t in triangles:
		p0 = normaliz(medium(t[1], t[2]))
		p1 = normaliz(medium(t[0], t[2]))
		p2 = normaliz(medium(t[0], t[1]))

		new_triangles.append((t[0], p1, p2))
		new_triangles.append((t[1], p0, p2))
		new_triangles.append((t[2], p0, p1))
		new_triangles.append((p0, p1, p2))

	return new_triangles

################################################################################
# printing

def print_circle(points):
	print('"circle": [')
	for i in range(0, len(points)-1):
		print_point2(points[i])
		print(',')
	print_point2(points[-1])
	print('\n]', end='')

def print_sphere(triangles):
	print('"sphere": [')
	for i in range(0, len(triangles)-1):
		print_triangle(triangles[i])
		print(',')
	print_triangle(triangles[-1])
	print('\n]', end='')

def print_triangle(triangle):
	print('[', end='')
	for i in range(0, len(triangle)-1):
		print_point3(triangle[i])
		print(', ', end='')
	print_point3(triangle[-1])
	print(']', end='')

def print_point2(point):
	print('[%f, %f]' % point, end='')

def print_point3(point):
	print('[%f, %f, %f]' % point, end='')

################################################################################
# Main

n = 6
if len(sys.argv) > 1:
	n = int(sys.argv[1])

circle = gen_circle(200 * n)
for i in range(1, n):
	triangles = divide_triangles(triangles)

print('{')
print_circle(circle)
print(',')
print_sphere(triangles)
print('\n}')

