////////////////////////////////////////////////////////////////////////////////
// Global variables

// Container for martices used during drawing
var g = new Object();

// Canvas contexts
var gl;
var ctx;

// State of drawing
var currentAngle = 0;
var incAngle = 0.7;
var interval = 0;

// Downloaded data
var sphere = new Array();
var circle = new Object();

// Gell-Mann matrices
var gmm = [
  [[1,0,0],[0,1,0],[0,0,1]],
  [[0,1,0],[1,0,0],[0,0,0]],
  [[0,'-i',0],['i',0,0],[0,0,0]],
  [[1,0,0],[0,-1,0],[0,0,0]],
  [[0,0,1],[0,0,0],[1,0,0]],
  [[0,0,'-i'],[0,0,0],['i',0,0]],
  [[0,0,0],[0,0,1],[0,1,0]],
  [[0,0,0],[0,0,'-i'],[0,0,'i']],
  [[1,0,0],[0,1,0],[0,0,-2]],
]

////////////////////////////////////////////////////////////////////////////////
// Main

var jq = jQuery.noConflict();
jq(document).ready(function() {
	// Create UI
	jq('#sidebar').accordion({
	        'active': 1,
	        'collapsible': true,
	        'autoHeight': false,
	    });

	var settings = jq('#settings');
	for (var i = 1; i < 9; ++i) {
		settings.append(
		        '<table class="activator" id="activator'+i+'" active="false">'+
		          '<tr><td>'+
		            '<div class="matrix">'+draw_matrix(i)+'</div>'+
		          '</td><td class="wide">'+
		            '<span class="slidertext">Axis no. '+i+':&nbsp;&nbsp;'+
		              '<span id="slidertext'+i+'">0</span>'+
		            '</span>'+
		            '<div><div class="slider" id="slider'+i+'"></div></div>'+
		          '</td>'+
		        '</tr></table>'
		    );

		// Create slider
		var widget = jq('#slider'+i);
		widget.slider({
		        'value':  0.0,
		        'min':   -0.95,
		        'max':    0.95,
		        'step':   0.05,
		    });
		widget.bind('slidechange', i, on_slider_change);

		// Bind activator
		var activator = jq('#activator'+i);
		activator.bind('click', i, on_activator_click);
	}

	jq('#activator1').attr('active', 'true');
	jq('#activator4').attr('active', 'true');
	jq('#activator6').attr('active', 'true');

	// Prepare canvas elements
	var canvasgl = document.getElementById('canvasgl');
	try {
		gl = canvasgl.getContext('experimental-webgl');
	} catch(err) {
		show_error('Could not initialize WebGL: ' + err);
		return;
	}

	var canvas2d = document.getElementById('canvas2d');
	try {
		ctx = canvas2d.getContext('2d');
	} catch(err) {
		show_error('Your browser does not support "canvas" element: ' + err);
		return;
	}

	// Reshaping
	reshape();
	jq(window).resize(reshape);

	// Prepare WebGL
	try {
		g.program = load_program();
	} catch(err) {
		show_error(err)
		return
	}

	setup(g.program);

	// Download data
	jq.ajax({
	        'url': '6.json',
	        'dataType': 'text',
	        'contentType': 'application/json',
	        'success': on_download_success,
	        'error': on_download_error,
	    });

	// Keep redrawing
	autorotation(true);
})

////////////////////////////////////////////////////////////////////////////////
// Drawing data

function on_download_success(result) {
	try {
		var data = jq.parseJSON(result);
	} catch(error) {
		show_error('Error while parsing data: ' + error);
		return;
	}
	circle.data = data['circle'];
	sphere = data['sphere'];

	update_plot();
}

function on_download_error(xhr, status, error) {
	show_error('Could not load data file: ' + error);
}

function update_plot() {
	// Show spinner
	show_spinner(true);

	// Update plot
	var num_active = get_axes().length;
	if (num_active == 3) {
		show_canvas2d(false);
		setTimeout(prepare_3d, 100);
		autorotation(true);
	} else {
		autorotation(false);
		show_canvasgl(false);
		setTimeout(prepare_2d, 100);;
	}
}

function prepare_2d(result) {
	var axes = get_axes();
	var slider_positions = get_slider_positions();
	var len = length(slider_positions); 
	circle.reshaped = new Array();

	// Helpers
	circle.radius1 = displacement_scale(len, 1.0);
	circle.radius2 = displacement_scale(len, 1.0 / Math.sqrt(3.0));
	circle.radius3 = displacement_scale(len, 0.5);

	// Reshaping circle
	for (var i = 0; i < circle.data.length; ++i) {
		var point = circle.data[i];
		var v = get_position(slider_positions, axes, point);
		var s = scale(v, len);
		circle.reshaped[i] = [s * point[0], s * point[1]];
	}

	// Prepare UI
	show_spinner(false);
	show_canvas2d(true);

	draw_2d();
}

function draw_2d() {
	var size = jq('#canvas2d').attr('width');
	var s = (0.95 * size) / 2.0;

	// Clear and save
	ctx.clearRect(0, 0, size, size);
	ctx.save();

	// Scale
	ctx.translate(size / 2.0, size / 2.0);
	ctx.scale(s, s);
	ctx.lineWidth = 2.0 / s;

	// Helpers 
	ctx.strokeStyle = '#FF0000';
	ctx.beginPath();
	ctx.arc(0.0, 0.0, circle.radius1, 0.0, Math.PI*2, true);
	ctx.closePath();
	ctx.stroke();

	ctx.strokeStyle = '#FFFF00';
	ctx.beginPath();
	ctx.arc(0.0, 0.0, circle.radius2, 0.0, Math.PI*2, true);
	ctx.closePath();
	ctx.stroke();

	ctx.strokeStyle = '#00FF00';
	ctx.beginPath();
	ctx.arc(0.0, 0.0, circle.radius3, 0.0, Math.PI*2, true);
	ctx.closePath();
	ctx.stroke();

	// New style 
	ctx.lineWidth = 3.0 / s;
	ctx.strokeStyle = 'rgb(20, 20, 20)'
	ctx.fillStyle = 'rgba(20, 20, 20, 0.2)';

	// Drawing shape
	ctx.beginPath();
	
	for (var i = 0; i < circle.reshaped.length; ++i) {
		var point = circle.reshaped[i];
		ctx.lineTo(point[0], point[1]);
	}

	ctx.closePath();

	ctx.fill();
	ctx.stroke();

	// Restore
	ctx.restore();
}

function prepare_3d(result) {
	var colors = new Array();
	var vertices = new Array();
	var normals = new Array();
	
	var axes = get_axes();
	var v = new Array();
	var x = new Array();
	var s = 0;

	for (var i = 1; i < 9; ++i) {
		v[i-1] = jq('#slider' + i).slider('value');
	}

	g.numItems = 3*sphere.length;
	for (var i = 0; i < sphere.length; ++i) {
		var triangle = sphere[i];

		for (var j = 0; j < 3; ++j) {
			point = triangle[j];

			for (var k = 0; k < 3; ++k) {
				v[axes[k]-1] = point[k];
			}

			s = scale(v, 0);

			if (s > 0.95) {
				colors.push(1.0, 0.0, 0.0, 0.5);
			} else if (s < 0.53) {
				colors.push(0.0, 1.0, 0.0, 0.5);
			} else if (s < 0.6) {
				colors.push(0.5, 1.0, 0.0, 0.5);
			} else {
				colors.push(1.0, 1.0, 0.0, 0.5);
			}

			vertices.push(s*point[0], s*point[1], s*point[2]);
			normals.push(point[0], point[1], point[2]);
		}
	}

	var colors = new Float32Array(colors);
	var vertices = new Float32Array(vertices);
	var normals = new Float32Array(normals);

	// Set up buffer for the colors
	g.cBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, g.cBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);
	gl.vertexAttribPointer(g.program.vColor, 4, gl.FLOAT, false, 0, 0);

	// Set up buffer for the vertices
	g.vBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, g.vBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(g.program.vPosition, 3, gl.FLOAT, false, 0, 0);

	// Set up buffer for the normals
	g.nBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, g.nBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, normals, gl.STATIC_DRAW);
	gl.vertexAttribPointer(g.program.vNormal, 3, gl.FLOAT, false, 0, 0);

	// Prepare UI
	show_canvasgl(true);
	show_spinner(false);

	draw_3d();
}

function draw_3d() {
	// Clear the canvas
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// Make a model/view matrix.
	g.mvMatrix.makeIdentity();
	g.mvMatrix.rotate(20, 1, 0, 0);
	g.mvMatrix.rotate(currentAngle, 0, 1, 0);

	// Construct the normal matrix from the model-view matrix and pass it
	g.normalMatrix.load(g.mvMatrix);
	g.normalMatrix.invert();
	g.normalMatrix.transpose();
	g.normalMatrix.setUniform(gl, g.u_normalMatrixLoc, false);

	// Construct the model-view * projection matrix and pass it in
	g.mvpMatrix.load(g.perspectiveMatrix);
	g.mvpMatrix.multiply(g.mvMatrix);
	g.mvpMatrix.setUniform(gl, g.u_modelViewProjMatrixLoc, false);

	// Draw
	gl.drawArrays(gl.TRIANGLES, 0, g.numItems);

	currentAngle += incAngle;
	if (currentAngle > 360) {
		currentAngle -= 360;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tools

function get_axes() {
	var n = 0;
	var axes = new Array();
	for (var i = 1; i < 9; ++i) {
		a = jq('#activator' + i);
		if (a.attr('active') == 'true') {
			axes[n] = i;
			n += 1;
		}
	}
	return axes;
}

function get_slider_positions() {
	var result = new Array();
	for (var i = 1; i < 9; ++i) {
		result[i-1] = jq('#slider' + i).slider('value');
	}
	return result;
}

function get_position(positions, axes, point) {
	var result = new Array();
	for (var i = 0; i < positions.length; ++i) {
		result[i] = positions[i];
	}
	for (var i = 0; i < 2; ++i) {
		result[axes[i]-1] = -point[i];
	}
	return result;
}

function draw_matrix(n, a) {
	var M = gmm[n];
	var result = '<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow>'
	if (n == 8) {
		result += '<mfrac><mrow>1</mrow><mrow><msqrt>3</msqrt></mrow></mfrac>';
	}
	result += '<mfenced open="[" close="]"><mtable>';
	for (var i = 0; i < M.length; ++i) {
		row = M[i];
		result += '<mtr>';
		for (var j = 0; j < row.length; ++j) {
			result += '<mtd><mn>' + row[j] + '</mn></mtd>';
		}
		result += '</mtr>';
	}
	result += '</mtable></mfenced></mrow></math>';
	return result;
}

////////////////////////////////////////////////////////////////////////////////
// UI

function reshape() {
	var size = 300;
	var frame_width = window.innerWidth;
	var frame_height = window.innerHeight;

	var canvas = jq('#canvasgl');
	var drawings = jq('.drawing');
	var sidebar = jq('#sidebar');

	if (2*size < frame_width) {
		var width_left = frame_width - size;
		var size = width_left < frame_height ? width_left : frame_height;
	}

	drawings.attr('width', size);
	drawings.attr('height', size);
	drawings.css('height', (size-12)+'px');
	drawings.css('width', (size-12)+'px');
	drawings.css('left', (frame_width-size-2)+'px')
	sidebar.css('width', (frame_width-size-12)+'px');

	gl.viewport(0, 0, size-12, size-12);
}

function on_slider_change(event, ui) {
	show_spinner(true);
	jq('#slidertext' + event.data).html(ui.value);
	update_plot();
}

function on_activator_click(event) {
	var activator = jq('#activator' + event.data);
	var state = activator.attr('active');
	var num_active = get_axes().length;
	
	// Change axies
	if (state == 'false') {
		if (num_active == 2) {
			activator.attr('active', 'true');
			update_plot();
		}
	} else {
		if (num_active == 3) {
			activator.attr('active', 'false');
			update_plot();
		}
	}
}

function show_spinner(show) {
	var spinner = jq('#spinner-container');
	if (show) {
		spinner.show();
	} else {
		spinner.hide();
	}
}

function show_canvasgl(show) {
	var canvas = jq('#canvasgl');
	if (show) {
		canvas.show();
	} else {
		canvas.hide();
	}
}

function show_canvas2d(show) {
	var canvas = jq('#canvas2d');
	if (show) {
		canvas.show();
	} else {
		canvas.hide();
	}
}

function autorotation(auto) {
	if (auto) {
		if (interval == 0) {
			interval = setInterval(draw_3d, 10);
		}
	} else {
		window.clearInterval(interval);
		interval = 0;
	}
}

function show_error(msg) {
	// Report error
	console.log(msg)
	jq('#error').html('ERROR: ' + msg);
	jq('.error-container').show();

	// Hide after 10 seconds
	setTimeout(function() { jq('#error').hide(); }, 10000);
}

////////////////////////////////////////////////////////////////////////////////
// Math functions

// Complex cubic root of coplex
function cbrt(c, d) {
	r = Math.pow(c*c + d*d, 1.0/6.0);
	t = Math.atan2(d, c) / 3.0;
	return [r * Math.cos(t), r * Math.sin(t)];
}

// Scalar product of 'n' and star product of 'n' and 'n'
function calc_nnn(n) {
//alert(n)
	var s3 = Math.sqrt(3.0);
	var n1 = n[0]; var n2 = n[1]; var n3 = n[2]; var n4 = n[3];
	var n5 = n[4]; var n6 = n[5]; var n7 = n[6]; var n8 = n[7];
	var n12 = n1*n1; var n22 = n2*n2; var n32 = n3*n3; var n42 = n4*n4;
	var n52 = n5*n5; var n62 = n6*n6; var n72 = n7*n7; var n82 = n8*n8;
	return 3.0*n8*(n12 + n22 + n32 - 0.5*(n42 + n52 + n62 + n72) - n82/3.0)
	     + 3.0*s3*(n1*n4*n6 + n1*n5*n7 + n2*n5*n6 - n2*n4*n7)
	     + 1.5*s3*n3*(n42 + n52 - n62 - n72);
}

// Check if x if approximately real in (0, 1)
function is_ok(x_re, x_im, r) {
	if (x_im < 0.001) {
		if (x_re > 0.0 && x_re < r) {
			return true;
		}
	}
	return false;
}

function scale(N, disp) {
	var s3 = Math.sqrt(3.0);
	var nnn = calc_nnn(N);

	if (nnn == 0.0) {
		return displacement_scale(disp, 1.0 / s3);
	}

	// frequently used variables
	var nnn2 = nnn * nnn;
	var a = -2.0 * nnn;
	var h1 = -1.0 / (      a);
	var h3 = -1.0 / (3.0 * a);
	var h6 = -1.0 / (6.0 * a);

	var e = -108.0 * nnn2 + 54.0;
	var f = 11664.0 * nnn2 * (nnn2 - 1.0);
	var sf_re = 0.0;
	var sf_im = 0.0;
	if (f >= 0) {
		sf_re = Math.sqrt(f);
	} else {
		sf_im = Math.sqrt(-f);
	}
	var sp = cbrt(0.5 * (e + sf_re),  0.5 * sf_im);
	var sm = cbrt(0.5 * (e - sf_re), -0.5 * sf_im);
	var sp_re = sp[0];
	var sp_im = sp[1];
	var sm_re = sm[0];
	var sm_im = sm[1];
	
	var segment_radius = 1.0;//displacement_scale(disp, 1.0);

	// x1
	var x1_re = h1 + h3 * (sp_re + sm_re);
	var x1_im =      h3 * (sp_im + sp_im);
	if (is_ok(x1_re, x1_im, segment_radius)) {
		return displacement_scale(disp, x1_re);
	}

	// fuv
	var pp_re = -h6;
	var pp_im = -h6 * s3;
	var pm_re = -h6;
	var pm_im =  h6 * s3;

	// x2
	var x2_re = h1 + pp_re*sp_re - pp_im*sp_im + pm_re*sm_re - pm_im*sm_im;
	var x2_im =      pp_re*sp_im + pp_im*sp_re + pm_re*sm_im + pm_im*sm_re;
	if (is_ok(x2_re, x2_im, segment_radius)) {
		return displacement_scale(disp, x2_re);
	}
		
	// x3
	var x3_re = h1 + pm_re*sp_re - pm_im*sp_im + pp_re*sm_re - pp_im*sm_im;
	var x3_im =      pm_re*sp_im + pm_im*sp_re + pp_re*sm_im + pp_im*sm_re;
	if (is_ok(x3_re, x3_im, segment_radius)) {
		return displacement_scale(disp, x3_re);
	}

	// fallback
	if (nnn == 1.0) {
		return displacement_scale(disp, 1.0);
	} else if (nnn == -1.0) {
		return displacement_scale(disp, 0.5);
	} else {
		return undefined;
	}
	// Yes, a bit complicated...
}

function length(vector) {
	var sum = 0.0;
	for (var i = 0; i < vector.length; ++i) {
		sum += vector[i] * vector[i];
	}
	return Math.sqrt(sum);
}

function displacement_scale(d, r) {
	// d - displacement
	// r - radius
	if (d < 1.0) {
		return r * Math.sin(Math.acos(d / r));
	} else if (d < 1.0001) {
		return r * Math.sin(Math.acos(1.0 / r));
	} else {
		return 0.0;
	}
}

////////////////////////////////////////////////////////////////////////////////
// Shaders

v = 'uniform mat4 u_modelViewProjMatrix;'
+'uniform mat4 u_normalMatrix;'
+'uniform vec3 lightDir;'
+''
+'attribute vec3 vNormal;'
+'attribute vec4 vColor;'
+'attribute vec4 vPosition;'
+''
+'varying float v_Dot;'
+'varying vec4 v_Color;'
+''
+'void main() {'
+'	gl_Position = u_modelViewProjMatrix * vPosition;'
+'	v_Color = vColor;'
+'	vec4 transNormal = u_normalMatrix * vec4(vNormal, 1);'
+'	v_Dot = 0.8 * dot(transNormal.xyz, lightDir) + 0.2;'
+'}'

f = 'precision mediump float;'
+''
+'varying float v_Dot;'
+'varying vec4 v_Color;'
+''
+'void main() {'
+'	gl_FragColor = vec4(v_Color.rgb * v_Dot, 1.0);'
+'}'

////////////////////////////////////////////////////////////////////////////////
// GL related functions

function load_shader(program, source, type) {
	var shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		console.log();
		throw "Error compiling shader: " + gl.getShaderInfoLog(shader);
	}

	gl.attachShader(program, shader);
	return shader;
}

function load_program() {
	var program = gl.createProgram();
	var vertexShader = load_shader(program, v, gl.VERTEX_SHADER);
	var fragmentShader = load_shader(program, f, gl.FRAGMENT_SHADER);

	gl.linkProgram(program);

	if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
		var error = gl.getProgramInfoLog(program);

		gl.deleteProgram(program);
		gl.deleteProgram(fragmentShader);
		gl.deleteProgram(vertexShader);

		throw "Error while linking shaders: " + error;
	}

	return program;
}

function setup(program) {
	// Frequently used variables
	program.vNormal = gl.getAttribLocation(program, "vNormal");
	program.vColor = gl.getAttribLocation(program, "vColor");
	program.vPosition = gl.getAttribLocation(program, "vPosition");

	gl.useProgram(program);

	gl.enableVertexAttribArray(program.vNormal);
	gl.enableVertexAttribArray(program.vColor);
	gl.enableVertexAttribArray(program.vPosition);

	// Prepare GL
	gl.clearColor(0.9, 0.9, 0.9, 1);
	gl.clearDepth(10000);

	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

	gl.uniform3f(gl.getUniformLocation(program, "lightDir"), 0, 0, 1);

	// Create some matrices to use later and save their locations in the shaders
	g.mvMatrix = new J3DIMatrix4();
	g.normalMatrix = new J3DIMatrix4();
	g.mvpMatrix = new J3DIMatrix4();
	g.u_normalMatrixLoc =
	        gl.getUniformLocation(g.program, "u_normalMatrix");
	g.u_modelViewProjMatrixLoc =
	        gl.getUniformLocation(g.program, "u_modelViewProjMatrix");

	g.perspectiveMatrix = new J3DIMatrix4();
	g.perspectiveMatrix.perspective(16, 1, 1, 10000);
	g.perspectiveMatrix.lookat(0, 0, 7, 0, 0, 0, 0, 1, 0);
}

